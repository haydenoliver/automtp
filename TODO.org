#+TITLE: To-do
#+DESCRIPTION: 
List of things to do to make this package useful and usable.

* New features 
** Setup 
*** DONE Setup config.yaml fiel
    Look at the .yaml from MatDB, try to figure out the best way to imitate that type of 
workflow, while being as agile as and generalized as possible
The config.yaml file should be able to specify all the inputs that the user wants 
to include, and should automatically build the databases of the 
*** TODO Create ENV
This needs to include the prerequisite modules, programs, and files to make the 
system run. 
Look into adding support for Docker, virtualenv, and Conda
**** Docker
**** VirtualEnv
**** 
*** IN-PROGRESS Pre-generate some of the structure files 
 There's not much of a need to pre-generate the structures. It's expensive to do so, and not
ver Probably Just up to the Enum.x step.
 
 However, by including a mapping of the structures, maybe we'd be able to generate the library 
of structures beforehand. 
**** TODO Look into using a mapping solution to use a pre-generated enumerated list of structures
*** IN-PROGRESS Build interactive project setup
    
    The interactive setup is aptly named, setup_interactive.py, and is in the setup folder. Soon though, 
these scripts will be moved to the a folder called src, along with everything else in the `bin` folder.


**** Interact with yaml input file
     The interactive setup will edit config entries in the system yaml file. Users will have the option
     of advanced or easy setups
*** Create a system-wide settings file for the computer to be used
    
** Train and build potential
*** Running
   Need a single script that can take over the jobs of the unwieldy bash scripts that we
currently use. Ideally, this will be in Python. I think that it is possible to do. 
**** Replace Step 0
**** Replace Step 1
**** Replace Step 2
**** Replace Step 3
**** Replace Step 4
     
*** Data recording
*** Job-state recording and recovery


** Plotting and Analysis
*** WAITING Binary convex hull
**** DONE Use python script to extract all data from relax.cfg
 assume that Matplotlib is the best option for what I'll be dealing with
, Julia may be better. Unfortunately, not many of the scientists here
 Julia.

 [[https://stackoverflow.com/questions/27270477/3d-convex-hull-from-point-cloud][Convex hull from point cloud]]

 OR use a prebuilt package in Python like the link above
*** TODO Ternary convex hull
    Use the same script fromt the binary hull to strip the data. 
Should be relatively simple to scale up to 3D, however, 
plotting in triangular coordinates is a pain in the butt.
*** TODO Phonons? Look at phonopy
** Data backup
*** IN-PROGRESS Make a more robust data backup and checkpointing system
* Improvements
** Streamline workflow
   Be able to run everything from a single command once the config yaml is finished.

** Remove redundant code
** Create 
