#!/bin/bash

#SBATCH --time=4:00:00   # walltime
#SBATCH --ntasks=1  # number of processor cores (i.e. tasks)
#SBATCH --nodes=1   # number of nodes
#SBATCH --mem-per-cpu=16GB   # total memory 
#SBATCH --array=1-4
####SBATCH --qos=msg
#SBATCH -C "avx2"



cd test_${SLURM_ARRAY_TASK_ID}

module purge
srun vasp6 | tee vasp.out


