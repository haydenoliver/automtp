from ase.db import connect
import pymatgen.analysis.phase_diagram as PD

with connect(database_path) as db:
    data = [PD.PDEntry(struct.formula, struct.energy) for struct in db.select()]

pd = PD.PhaseDiagram(data)
pd_plot = PD.PDPlotter(pd, show_unstable=.05)
pd_plot.write_image('convex_hull.svg', image_format=svg, label_unstable=False)
