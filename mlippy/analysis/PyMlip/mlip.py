from build import MTP
import numpy


class Mlip:
    """
    This class creates an object containing all the information present in
    the mlip config files. It parses the given config file, and stores the
    data as attributes of an object, accessible through standard python
    class-object structure.

    This creates one object per config, therefore, in order to build an
    object for each structure in a large mlip.cfg file, you must use the
    'parseCFG'

    If you want to create objects for each file


    """

    source_format = 'mlip'

    def __init__(self):  # , cfg_data):#, mapping):
        """Class instance initialization"""
        pass

    def read_CFG(self, config, species, species_map):
        cfg_filtered = list(filter(None,
                            config.strip('BEGIN_CFG'
                            ).strip("END_CFG"
                            ).split('\n'))
        )
        self.species = species
        self.cell_vecs = []
        self._atom_data = {}
        self.features = {}
        self.positions = []
        self.forces = []

        line_num = 0

        for line in cfg_filtered:
            line_num += 1

            if 'Size' in line:
                self.cell_size = int(cfg_filtered[line_num])

            if 'Supercell' in line:
                self.cell_vecs.append([float(i) for i in cfg_filtered[line_num + 0].split()])
                self.cell_vecs.append([float(i) for i in cfg_filtered[line_num + 1].split()])
                self.cell_vecs.append([float(i) for i in cfg_filtered[line_num + 2].split()])

            if 'AtomData' in line:
                _atom_species = []
                _positions = []
                _forces = []

                for i in range(self.cell_size):
                    adata = [float(f) for f in cfg_filtered[line_num + i].split()]
                    _atomNum = int(adata[0])
                    _atomType = int(adata[1])
                    _atomCoord = adata[2:5]
                    _atomForce = adata[5:8]

                    _atom_species.append(_atomType)

                    _positions.append(_atomCoord)
                    _forces.append(_atomForce)

                self.forces = _forces
                # print(species_map)
                #                print(_atom_species, (species_map))
#                print(_atom_species,species_map)
                #                if _atom_species[0]:
                _ATOM_SPECIES = [species_map[k] for k in _atom_species]
#                print(_ATOM_SPECIES)
#               else:
 #                   _ATOM_SPECIES = _atom_species
 #               print(_ATOM_SPECIES)
                _species, _indices, _quant = numpy.unique(
                                        _ATOM_SPECIES,
                                        return_counts=True,
                                        return_inverse=True
                                    )
 
                self.positions = _positions
                self.species_struct = _species

                self._species_indices = _indices
                self.species_list = _species[_indices]
                #                print(self.species_list)

                if not sum(_quant) == self.cell_size:
                    print("ERROR (cell size disagreement)")
                    print("Check cfg:", self.conf_id)
                    sys.exit()

                self._formula = _quant

                
                self.nspecies_struct = len(_species)
                self.pure = (self.nspecies_struct == 1)
#                print(self.nspecies_struct, len(self.species))

                #binary only                
                if len(self.species) == 2:
                    # BINARY CASE
                    countB = list(self.species_list).count(self.species[1])
                    countAB = len(self.species_list)
                    percentB = float(countB / countAB)
                    self.composition = percentB
                    continue
                elif len(self.species) == 3:
                    # TERNARY CASE
                    # Uses triangular coordinate conversion found on Wikipedia:
                    # https://en.wikipedia.org/wiki/Ternary_plot#Plotting_a_ternary_plot
                    
                    
                    concs = {}

                    for i in range(len(self.species)):
                        elem = self.species[i]
                        concs[i] = (float(list(self.species_list).count(elem) /
                                             len(self.species_list)))

                    # concs (dict): access the concentration value with the assigned number of the
                    # atomic species
                    a = concs[0]
                    b = concs[1]
                    c = concs[2]


                    x_coord = 0.5*(2*b+c)/(a+b+c)
                    y_coord = 0.5*numpy.sqrt(3) * c/(a+b+c)

                    self.composition = concs
                    print(concs)
                    self.cartesian = (x_coord, y_coord)
                    
#                    print(self.cartesian)
            if 'Energy' in line:
                self.energy = float(cfg_filtered[line_num])
                continue
                
            if 'PlusStress' in line:
                self.plusstress = [float(i) for i in cfg_filtered[line_num].split()]
                continue
            
            if 'conf_id' in line:
                self.conf_id = int(cfg_filtered[line_num -1].split()[2])
            
            if 'Feature' in line:
                feat_type = cfg_filtered[line_num - 1].split()[1]
                feat_val = cfg_filtered[line_num - 1].split()[2]
                self.features[feat_type] = feat_val
                continue
        return
    
    
    
    
    
#     def get_forces(self):
#         """Returns the force vectors corresponding to each atom in the system"""
#         self.forces = {}
#         for a in self._atom_data:
#             self.forces[a] = self._atom_data[a]['forces'] 
         
#         return self.forces
    
    
    
    
    # =================================================================== #
    # These functions can return and store different types of attributes, #
    # but they are not executed in the object initialization.             #
    # =================================================================== #
    
    # get
    
    def _set_species(self, new_species_list, sorting=True):
        """
        If the species IDs taken from the config files are set to digit identifiers,
        you can change the ID of the elements by inputing a list of the same length 
        as the original species list. 
        
        Element IDs should ordered alphabetically when enumerated by makeStr.py, 
        therefore, in order to map the correct names to the right properties, this 
        will sort your list automatically unless directed otherwise with 'sorting=False'.
        
        Args:
            new_species_list (list): A list of new identifiers for the elements in the
                        system. It is best to set these either the chemical
                        symbols (str) or the atomic number (int) of the elements.
            
            sort (bool, default=True): If True, new_species_list will be sorted using 
                        the numpy.sort() method
                            ==== CAUTION ====
                        'False' will break your setup if you have mixed up the order of 
                        the elements. 
                        
                        
        Returns:
            self.species (list): A new list of identifiers for the elements in the system.
            
        
        Examples1:
            >>> self.species
            [0, 1, 2]
            >>> set_species(['Co', 'Al', 'W'])     # unsorted list
            >>> self.species
            ['Al', 'Co', 'W']
            
        Example 2:
            >>> self.species
            [0, 1, 2]
            >>> set_species(['Co', 'Al', 'W'], sorting=False)  
            >>> self.species
            ['Co', 'Al', 'W']
            
            
            
        Mapping:
            original IDs:    [ 0,    1,    2 ]
                               |     |     |
            new Ids:         ['Al', 'Co', 'W']         
        
        """
        
#         for elem in new_species_list:
            
#         if _atom_species[0].isdigit():
#             _ATOM_SPECIES = [species_map[k] for k in numpy.arange(self.nspecies)]
#         else:
#             _ATOM_SPECIES = _atom_species

#         print(_ATOM_SPECIES)
#         _species, _indices, _quant = numpy.unique(
#                                 _ATOM_SPECIES,
#                                 return_counts=True,
#                                 return_inverse=True
#                             )

        self.positions = _positions
        self.species = _species
#                 print(_species)
        self._species_indices = _indices
        self.species_list = _species[_indices]

        if not sum(_quant) == self.cell_size:
            print("ERROR (cell size disagreement)")
            print("Check cfg:", self.conf_id)
            sys.exit()
        
        # self.natoms = sum(_quant)
        self._formula = _quant
        self.nspecies = len(_species)
        self.pure = self.nspecies == 1
        
        
        
        
        
        [species_map[k] for k in _atom_species]
        
        if sorting == True:
            _new_list = numpy.sort(new_species_list)
            
        elif sorting == False:
            _new_list = new_species_list
        
        self.species = _new_list
        self.species_list = _new_list[self._species_indices]
        
        return

# Unnecessary. Now inheriting values from parent class
#     def set_species_map(self, mapping):
#         species_map =  species_map() mapping
    
    def get_ase(self, save=False):
        """Build an ase.Atoms object using the data stored in the Mlip object.
        If 'save=True', then this will save the formula as an attribute to the 
        instance of this class.
        """
        
        ase_atoms = Atoms(symbols=self.species_list, cell=self.cell_vecs, positions=self.positions, pbc=True)

        if save == True:
            self.ase = ase_atoms
        
        return ase_atoms 
    
#     def store_ase(self): deprecated
#         """Stores the ASE atoms object as an attribute of the Mlip object"""
#         self.ase = self.get_ase()
    
    def get_chemical_formula(self, save=False, rename=True):
        """Outputs the chemical formula the structure
        If 'save=True', then this will save the formula as an attribute to the 
        instance of this class.
        If the 
        """
        try:
            formula = ''.join(numpy.transpose([self.species, self._formula]).flatten())
        except TypeError:
            print("Error: Please change the species IDs to strings")
            print("Pass a list of chemical symbols into obj.set_species()")
            return
            
            
        if save == True:
            self.formula = formula
        
        return formula
    
    # def set_energy_map(self, energy_map):
        # """Takes key:value pairs for mapping the lowest energy/atom for pure structures
        # """
        # if type(energy_map) == dict:
            # self.energy_map = energy_map
   
        
    def calc_formation_enthalpy(self, energy_map, save=False):
        """
        Determines the formation enthalpy of the structure.
        If 'save=True', then this will save the formula as an attribute to the 
        instance of this class.
        
        
        Args:
            pures (dict): The reference structures and their corresponding
                        energies.
            per_atom (bool, default=False): Determines if the calculator will
                        return the total formation enthalpy (True), or the total
                        formation enthalpy per atom (False). 
        Returns:
            dH (float): Calculated formation enthalpy in meV (or meV/atom if per_atom=False)
        """
        # self.energy_map = energy_map

        Hatom = 0
        Htot = 0
        for elem in self.species_list:
            Hatom = Hatom + energy_map['atom'][elem]
            Htot =  Htot + energy_map['total'][elem]

        E = self.energy
        
        # print(E, H)
        # if chull_type == 'total':
        _dH_total = (E - Htot) * 1000 # (gotta convert to meV)
        self.dH_total = _dH_total

        # elif chull_type == 'atom':
        _dH_per_atom = (E - Hatom) / self.cell_size * 1000 # (gotta convert to meV)
        # print(self.energy
        self.dH_per_atom = _dH_per_atom

#        print(self.dH_per_atom, self.dH_total)

    def print_cfg(self):
        print('BEGIN_CFG')
        print(" Size")
        print(" ", self.cell_size)

        print(" Supercell")
        for vec in self.cell_vecs:
            vec_str = '\t'.join(list(map(str, vec)))
            print("        ", vec_str)

        print(" AtomData:  id\t type\t cartes_x\t cartes_y\t cartes_z\t\t fx\t\t fy\t\t fz")
        for i in range(self.cell_size-1):
            pos_str = '\t'.join(list(map(str, self.positions[i])))

            frc_str = '\t  '.join(list(map(str, [numpy.format_float_scientific(val) for val in self.forces[i]])))

            print('      \t', i, ' ', self.species_list[i], '\t', pos_str, '\t  ', frc_str)

        print(" Energy")
        print('\t', self.energy)

        print(" PlusStress:  xx          yy          zz          yz          xz          xy")
        print('\t', '\t'.join(list(map(str, numpy.round(self.plusstress, 6)))))

        for feat_type in self.features:
            print(" Feature ", feat_type, '\t', self.features[feat_type])
        print("END_CFG")
