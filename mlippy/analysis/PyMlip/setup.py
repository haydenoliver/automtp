from setuptools import setup, find_packages

setup(
    name='pymlip',
    version='0.1',
    description
      ='Easy integration with config outputs from Mlip',
    long_description
      ='Builds objects for each configuration organizing its data in an easily accessible format. ',
    classifiers
    =[
        'Development Status :: 3 - Alpha',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3.7',
        'Topic :: Computational Materials :: Active Learning',
    ],
    keywords
      ='mtp mlip active learning dft vasp convex hull phase diagram',
    url
      ='https://gitlab.com/haydenoliver/automtp.git',
    author
      ='Hayden Oliver',
    author_email
      ='haydenoliver@physics.byu.edu',
    license
      ='MIT',
    packages
      =find_packages(),
    install_requires
    =[
        # 'numpy',
	    # 'datetime',
	    'matplotlib',
	    'ase',
            'pyyaml',
        # 'pickle'
    ],
    include_package_data
      =True,
    zip_safe
      =False)
