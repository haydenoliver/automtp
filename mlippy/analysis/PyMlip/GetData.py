import os
import pickle

# import sys
import yaml
import build

import argparse

"""
Run this script to prepare for plotting.
"""
parser = argparse.ArgumentParser()
datasource = parser.add_mutually_exclusive_group()

# Retrieving data

datasource.add_argument('-p', '--pkl_load',
                        help='Read in pkl file from previous analysis',
                        # metavar='',
                        # action='store_true'
                        metavar=''
                        )
parser.add_argument('-r', '--read_cfg',
                    help='Filename of the file containing the Mlip relaxed structures',
                    metavar=''
                    )

# Generating data
# parser.add_argument('-ch', '--convex_hull',
                    # help='creates a O K convex hull using total energies ("total") OR energy per atom ("atom")',
                    # nargs='?',
                    # const='total',
                    # choices=['total', 'atom']
                    # default='total',
                    # action='store_true',
                    # type=str,
                    # metavar=''
                    # )
parser.add_argument('-y', '--yaml',
                    help='specifies which yaml file will be read in.\nDefault is "system.yaml"',
                    metavar='')
# parser.add_argument(''
# )
parser.add_argument('-w', '--write_file',
                    help='write the config data to file',
                    metavar='')
datasource.add_argument('-b', '--build_MTP',
                        help='Creates an instance of build.MTP',
                        # metavar='',
                        default=True,
                        action='store_true')

# Viewing data
parser.add_argument('-s', '--show',
                    action='store_true',
                    help='Show plots and data generated'
                    )

args = parser.parse_args()


# Read in .yaml configuration file from same dir as config file

# Create the build.MTP object that contains the data from the input file
# print(args.pkl_load)
if not (args.pkl_load or args.build_MTP):
    parser.error('No data input given. add --pkl_load or --build_MTP')

if args.pkl_load:
    print('reading pkl')
    with open(args.pkl_load, 'rb') as infile:
        cfgs = pickle.load(infile)
    path = cfgs.path
    cfgs.n_configs = len(cfgs.configs)
    yaml_path = cfgs.yaml_path
    with open(yaml_path) as data:
        yml = yaml.safe_load(data)

elif args.build_MTP:
    # Set filepath for the relaxed config list
    print('Building MTP')
    if args.yaml:
        print('reading yaml from:', args.yaml)
        yaml_path = args.yaml
    else:
        yaml_path = 'system.yaml'

    with open(yaml_path) as data:
        yml = yaml.safe_load(data)
    # print('building cfg')
    if args.read_cfg:
        print('reading path from args')
        cfg_path = os.path.abspath(args.read_cfg)
    else:
        print('reading path from yaml')
        cfg_path = os.path.join(yml['path']['ROOT'],yml['path']['struct_path'], yml['output']['relax'])

    cfgs = build.MTP()
    cfgs.set_path(inputpath=cfg_path)
    cfgs.set_yaml_path(yaml_path)
#    cfgs.set_wspecies_map(yml['species'], mapping=yml['mapping'])
    cfgs.set_species_map(yml['species'])
    print(cfgs.species_map, " ====== MAP")
#    quit()
    cfgs.read_configs()
    cfgs.build_Mlip()

cfgs.set_pure_energy_ref()
f_enthalpy = cfgs.get_formation_enthalpies()

# Export the cfgs object to a json object for plotting and interpreting
import json

fileout = os.path.join(yml['path']['ROOT'], yml['path']['struct_path'], 'cfgs.json')

cfg_json = {}
for struct in cfgs.configs:
    struct_json = {}
    struct_json['composition'] = struct.composition
    print(struct.composition)
    struct_json['cartesian'] = struct.cartesian
    struct_json['_dH_per_atom'] = struct.dH_per_atom
    cfg_json[int(struct.conf_id)] = struct_json
    
with open(fileout, 'w') as f:    
    json.dump(cfg_json, f)

#cfgs.plot_formation_enthalpies(f_enthalpy, show=args.show)
#cfgs.get_convex_hull(f_enthalpy)

#if args.read_cfg:
#    filename = args.read_cfg.split('_')[0]
#elif args.pkl_load:
#    filename = args.pkl_load.split('_')[0]
#else:
#    filename = 'default_output'

#print('nconfigs:', type(cfgs.n_configs))
#print(yml['path'])
#outdir = os.path.join(yml['path'], yml['output']['analysis']['getdata'])
#outpath = os.path.join(outdir, filename + '_' + str(cfgs.n_configs) + '.pkl')
#print(outpath)
#print(os.getcwd())
#with open(outpath, 'wb') as fout:
#    pickle.dump(cfgs, fout)

# file_cfgs = open('cfgs.pkl', 'w')
# pickle.dump(cfgs, file_cfgs)
