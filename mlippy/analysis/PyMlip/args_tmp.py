import argparse
parser = argparse.ArgumentParser()

# Defines arguments for the parser
parser.add_argument("-e", "--echo",
                    help="Prints to screen the user's input")
parser.add_argument("-t", "--type",
                    help="Prints the type of variable given by the user")

args = parser.parse_args()
if args.echo:
    print(args.echo)
if args.type:
    print(args.type)
    # print(type(args.type))
