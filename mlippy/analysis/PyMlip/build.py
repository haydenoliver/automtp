# Standard library imports
from datetime import datetime
import os
from matplotlib import pyplot as plt
import numpy
# import sys

# 3rd party imports (from pip)

# from ase import Atoms, Atom, data


class MTP:
    # Class variables
    date_created = datetime.now()

    def __init__(self):
        # self.script_path = #, cfg_path, mapping=''):
        print("Initializing Configs object")

    def set_path(self, inputpath='relaxed.cfg'):
        self.path = os.path.abspath(inputpath)

    def read_configs(self):
        """Reads in the mlip.cfg file. Filters and splits the list so that
        each config is an element of a list"""

        with open(self.path) as cfg:
            self._config_list = list(
                filter(None, cfg.read(
                       ).strip('END_CFG'
                       ).replace('\t', '  '
                       ).strip('\n\n'
                       ).split('BEGIN_CFG')
                )
            )

        self.n_configs = len(self._config_list)
#         self.config_list = config_list

    def build_Mlip(self):
        # Create all of the config objects and add them to configs
        import mlip

        self.configs = [None] * self.n_configs
        for i in range(self.n_configs):
            self.configs[i] = mlip.Mlip()#self._config_list[i])
                                        
            self.configs[i].read_CFG(self._config_list[i],
                                     species=self.species,
                                     species_map=self.species_map
                                    )
        self._config_list = None

# ================ END SETUP FUNCTIONS ##############
    def set_yaml_path(self, yaml_path):
        self.yaml_path = yaml_path

    def set_pure_energy_ref(self):
        """
        Searches the config files for structures consisting purely of 
        one element. Finds the most lowest energy configuration of the
        pure element. Creates a dictionary for reference during energy
        formation enthalpy calculations. 
        Units of the values in the dictionary are  'meV/atom'.
        
        Args:
            self.configs (Configs attribute):
                    Contains all the structures that will be searched 
                    in this function.

        Returns:
            self.pures (dict): 
                    Lowest pure energies referenced by the chemical symbol.

        Uses:
            Mlip Object: species, energy, natoms

        """
        _pures = []
        # self.chull_type = chull_type
        for struct in self.configs:
            if struct.pure is True:
                _pures.append(struct.conf_id)

        # Set up energy mapping
        species = self.species
   #     print(species,type(species))

        import copy
        e_ref = ({
            chem: float(num) for (chem, num) in
            numpy.transpose([species, numpy.zeros(len(self.species_map))])}
        )
  #      print(e_ref)

        # Need to make a deepcopy of e_ref, otherwise the pointers are copied as well
        e_ref2 = copy.deepcopy(e_ref)
        energy_reference = {'atom':e_ref, 'total':e_ref2}
 #       print("ENERGY REFERENCE", energy_reference)
        for struct in _pures:
#            print(struct,type(struct))
            _elem = self.configs[struct].species_list[0]
            _energy = self.configs[struct].energy
            _n_atoms = self.configs[struct].cell_size
            # print(_elem, _energy, _n_atoms)

            eTot = _energy
            eAtom = _energy / _n_atoms
#            print(energy_reference,_elem, self.configs[struct].__dict__)

            eElemTot = energy_reference['total'][_elem]
            eElemAtom = energy_reference['atom'][_elem]

            # print(energy_reference)
            if eTot < eElemTot:
                energy_reference['total'][_elem] = eTot

            if eAtom < eElemAtom:
                energy_reference['atom'][_elem] = eAtom
#        print(energy_reference)
        self.energy_reference = energy_reference
 
    def get_formation_enthalpies(self):#, Total=True):
        formation_enthalpy = []
        e_ref = self.energy_reference

        for struct in self.configs:
            struct.calc_formation_enthalpy(e_ref)
            struct.calc_formation_enthalpy(e_ref)

            formation_enthalpy.append([struct.composition, struct.dH_per_atom, struct.dH_total])
        # print(formation_enthalpy)
        return formation_enthalpy

    def plot_formation_enthalpies(self, f_enthalpy, save=True, show=False):#, method='LOCAL'):
        composition = numpy.asarray(f_enthalpy)[:, 0]
        energy_atom = numpy.asarray(f_enthalpy)[:,1 ]
        energy_total = numpy.asarray(f_enthalpy)[:, 2]
        
        # Binary plot only for now
        plt.scatter(composition, energy_atom, c='b', alpha=.5, s=2)
        if save is True:
            title = 'chull_per_atom_' + str(self.n_configs) + '.png'
            plt.title('0 K Enthalpy vs concentration')
            plt.xlabel('Parts Zn')
            plt.ylabel('Formation enthalpy per atom\n (meV/atom)')
            plt.savefig(title)
        if show is True:
            plt.show()

#        # plt.scatter(composition, energy_total, c='b', alpha=.5, s=2)
#        # if save is True:
#            # title = 'chull_total_' + str(self.n_configs) + '.png'
#            # plt.title('0 K Total Energy vs concentration')
#            # plt.xlabel('Parts Zn')
#            # plt.ylabel('Total energy of the system')
#            # plt.savefig(title)
#        # if show is True:
#            # plt.show()

# plt.show()

    def get_convex_hull(self, f_enthalpy):
        from scipy.spatial import ConvexHull
        import numpy
        points = numpy.asarray(f_enthalpy)[:, [0, 1]]
        hull = ConvexHull(points)

        plt.plot(points[:, 0], points[:, 1], '.')
        for simplex in hull.simplices:
            plt.plot(points[simplex, 0], points[simplex, 1], 'k-', c='b')

        plt.savefig('chull.png')
        plt.show()

    def set_species_map(self, species, mapping=''):
        """
        Takes space separated string of chemical symbols, or list-type.
        If inputed as a string, the symbols/numbers must be space separated.

                -- MUST BE VALID CHEMICAL NUMBERS/SYMBOLS --

        Sorts and parses the input, mapping the numbered structures to a
        chemical symbol. CAUTION, elements will be ordered alphabetically.

        Args:
            species (list or string): A list of either chemical symbols or
                        atomic numbers.
                    Ex.1:    'Al Co W'
                    Ex.2:    '13 27 74'
                    Ex.3:    ['Al', 'Co', 'W']
                    Ex.4:    [13, 27, 74]
                    Ex.5:    ['13', '27', '74']

            mapping (list or string): A list of the map values outputted from
                        MLIP config file generation in makeStr.py from Enumlib.
                        Must be integer values. Must match the "--species_mapping"
                        tag used by makeStr.py during the config creation.
                        
                    makeStr default (binary system): ['0', '1']

                    Option 2: (binary): ['27', '74']  (using the atomic numbers as labels)

        Defines:
            self.system (list): An alphabetically ordered list of the chemical
                        symbols of each element present in the system

            self.species_map (dict): A dictionary mapping the
        """
        
 #       from ase.data import chemical_symbols
        
 #       chem_sym = chemical_symbols[1:-1]


        # species = self.species
        # print("SPECIES:", species, type(species))
        # print("MAPPING:", mapping, type(mapping))
#        print(species, " ++++++++++ SPECIES")
        self.species_map = species
        self.species = [v for k,v in species.items()]
#        print(self.species, self.species_map)
#        for i in self.species_map:
#            print(i,self.species_map[i])
        #       print(self.species)
        return # removing the code that follows this




#         if type(species) == str:
#             _species = numpy.sort(species.split())
#         else:
#             print(species,type(species))
#             _species = numpy.sort([v for k,v in species.items()])

#         elements = []

#         for elem in _species:
#             try:
#                 elements.append(chem_sym[int(elem)])
#             except ValueError:
#                 if elem not in chem_sym:
#                     raise Exception(
#                         "Please provide valid chemical symbols or atomic numbers")
#                     quit()
#                 else:
#                     elements.append(elem)

#         if mapping == '':
#             _mapping = numpy.arange(len(elements))
#         else:
#             _mapping = mapping

#         # _mapping = numpy.arange(len(elements))
#         new_mapping = {num: chem for (chem, num) in numpy.transpose(
#             [elements, _mapping])}

# #         self.chem_map = new_mapping
# #         try:
# #             for struct in self.configs:
# #                 struct.species_map = new_mapping
# #         except:
# #             print('')
#         self.species_map = new_mapping

#         self.species = _species

#         return

    # ==========================================================
    # RETURN ELEMENTS OF THE OBJECT
    # ==========================================================

    def return_species_map(self):
        return self.species_map

    def return_species(self):
        return self.species
