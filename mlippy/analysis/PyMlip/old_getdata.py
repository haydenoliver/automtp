import os
import pickle
# import sys
import yaml

# from build import MTP
# from mlip import Mlip

import build
# import mlip


import argparse
parser = argparse.ArgumentParser()
datasource = parser.add_mutually_exclusive_group()

# Retrieving data

datasource.add_argument('-p', '--pkl_load',
                        help='Read in pkl file from previous analysis',
                        # metavar='',
                        # action='store_true'
                        metavar=''
                        )
parser.add_argument('-r', '--read_cfg',
                    help='Filename of the file containing the Mlip relaxed structures',
                    metavar=''
                    )

# Generating data
# parser.add_argument('-ch', '--convex_hull',
                    # help='creates a O K convex hull using total energies ("total") OR energy per atom ("atom")',
                    # nargs='?',
                    # const='total',
                    # choices=['total', 'atom']
                    # default='total',
                    # action='store_true',
                    # type=str,
                    # metavar=''
                    # )
parser.add_argument('-y', '--yaml',
                    help='specifies which yaml file will be read in.\nDefault is "system.yaml"',
                    metavar='')
# parser.add_argument(''
# )
parser.add_argument('-w', '--write_file',
                    help='write the config data to file',
                    metavar='')
datasource.add_argument('-b', '--build_MTP',
                        help='Creates an instance of build.MTP',
                        # metavar='',
                        default=True,
                        action='store_true')

# Viewing data
parser.add_argument('-s', '--show',
                    action='store_true',
                    help='Show plots and data generated'
                    )

args = parser.parse_args()


# Read in .yaml configuration file from same dir as config file

# Create the build.MTP object that contains the data from the input file
# print(args.pkl_load)
if not (args.pkl_load or args.build_MTP):
    parser.error('No data input given. add --pkl_load or --build_MTP')

if args.pkl_load:
    print('reading pkl')
    with open(args.pkl_load, 'rb') as infile:
        cfgs = pickle.load(infile)
    path = cfgs.path
    yaml_path = cfgs.yaml_path
    with open(yaml_path) as data:
        yml = yaml.safe_load(data)

elif args.build_MTP:
    # Set filepath for the relaxed config list
    print('Building MTP')
    if args.yaml:
        print('reading yaml from:', args.yaml)
        yaml_path = args.yaml
    else:
        yaml_path = 'system.yaml'

    with open(yaml_path) as data:
        yml = yaml.safe_load(data)
    # print('building cfg')
    if args.read_cfg:
        print('reading path from args')
        path = os.path.abspath(args.read_cfg)
    else:
        print('reading path from yaml')
        path = os.path.join(yml['path'], yml['output']['relax'])

    cfgs = build.MTP()
    cfgs.set_path(inputpath=path)
    cfgs.set_yaml_path(yaml_path)
    cfgs.set_species_map(yml['species'], mapping=yml['mapping'])

    cfgs.read_configs()
    cfgs.build_Mlip()

# print(args.convex_hull)
# if args.convex_hull:
    # print(args.convex_hull)
cfgs.set_pure_energy_ref()
f_enthalpy = cfgs.get_formation_enthalpies()
cfgs.plot_formation_enthalpies(f_enthalpy, show=args.show)
cfgs.get_convex_hull(f_enthalpy)
print(cfgs.energy_reference, cfgs.energy_atom, cfgs.energy_total)
    # if args.convex_hull is True or args.convex_hull.lower() == 'total':
    # cfgs.set_pure_energy_ref(chull_type='total')
    # cfgs.get_formation_enthalpies()
    # cfgs.plot_formation_enthalpies()


    # elif args.convex_hull.lower() == 'atom':
        # cfgs.set_pure_energy_ref(chull_type='atom')
        
    # else:
        # print("ERROR in '--convex_hull'. Expected args 'total' or 'atom'")
# quit()


# cfgs.set_pure_energy_ref(Total=False)
# print(cfgs.energy_reference)


# Write data to file
# outpath = os.path.join(
    # yml['path'],
    # yml['output']['analysis']['getdata'] + str(cfgs.n_configs) + '.pkl')
outpath = (yml['path'] +
           args.read_cfg + '_' +
           str(cfgs.n_configs) +
           '.pkl')
print(outpath)
print(os.getcwd())
with open(outpath, 'wb') as fout:
    pickle.dump(cfgs, fout)

# file_cfgs = open('cfgs.pkl', 'w')
# pickle.dump(cfgs, file_cfgs)
