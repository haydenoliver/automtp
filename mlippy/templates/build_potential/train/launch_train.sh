#!/bin/bash

#SBATCH --nodes=1
#SBATCH --ntasks=24
#SBATCH --time=4:00:00
#SBATCH --mem-per-cpu=1GB
#SBATCH --partition=physics
#SBATCH -J 'Train'
##SBATCH -C 'avx2'
##SBATCH --qos=msg

module purge
module load gcc/8 openmpi/4.0

srun mlp train curr.mtp train.cfg --max-iter=500 --trained-pot-name=curr.mtp --curr-pot-name=curr.mtp --stress-weight=5e-4 --force-weight=5e-3
