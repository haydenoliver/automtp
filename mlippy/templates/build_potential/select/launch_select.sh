#!/bin/bash

#SBATCH --nodes=1 
#SBATCH --ntasks=16
#SBATCH --time=2:00:00
#SBATCH --mem-per-cpu=1GB
#SBATCH -J 'Select'
#SBATCH -C 'knl'
#SBATCH --qos=msg
##SBATCH -p 'physics'

module purge
module load gcc/8 openmpi/4.0

mlp select-add curr.mtp train.cfg preselected.cfg selected.cfg --select-threshold=2.0 --nbh-weight=0.0 --energy-weight=1.0 --mvs-filename=state.mvs --selected-filename=active_set.cfg 
#--selection-limit=100


