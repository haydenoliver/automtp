#!/bin/bash

#SBATCH --time=4:00:00   # walltime
#SBATCH --ntasks=48   # number of processor cores (i.e. tasks)
###SBATCH --cpus-per-task=1
#SBATCH --nodes=2   # number of nodes
#SBATCH --mem-per-cpu=512MB   # memory per CPU core
#SBATCH -J "HfNiTi-r"   # job name
#SBATCH -p 'physics'
##SBATCH -C 'knl'
##SBATCH --qos=msg

# Get the path to the executable; should be on user's path after the modules have been loaded.
#module purge
module load gcc/8 openmpi/4.0
#module load intel-mkl/2017 intel-compilers/2017

srun mlp relax mlip.ini --force-tolerance=1e-3 --stress-tolerance=1e-2 --max-step=0.03 --cfg-filename=to-relax.cfg --save-relaxed=relaxed.cfgsr

