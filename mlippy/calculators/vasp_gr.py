import subprocess as sp
import os


from ase.calculators.vasp import Vasp2 as V2
from ase.calculators.vasp.create_input import GenerateVaspInput as gvi
from ase.calculators.calculator import Calculator

"""
This script creates an ASE Calculator object that is compatible with the 
AutoMTP package. 
"""


class GenerateVaspInput(gvi):
    def write_kpoints(self, directory='./', **kwargs):

        """
        Writes the KPOINTS file. Overrides the default ASE kpoint generator 
        
        Specify what type of kpoint mesh to use for the calculation.

        Args:
        kpt_type (str): Specify the kpoint mesh types. 
            Available options:
               * "kgridgen": Runs the Generalized Regular Kpoint-grid Generator
                             from msg-byu.github.com/grkgridgen
               * "mueller_s": Mueller GR kpoint grid server
               * "mueller_j": Muller GR kpoint grid java install
               * "mp": Monkhorst-Pack kpoint grids -- Calls the ase.calculator.vasp 
                       kpoint generator
        """

        kpdensity = 1600

        kpgen_path = os.path.join(directory, "KPGEN")
        
        # Write KPGEN file
        with open(kpgen_path, 'w') as f:
            print("KPDENSITY=" + str(kpdensity), file=f)

        sp.run(['.'], executable="kpoints.x", cwd=directory)
               
class Vasp2(GenerateVaspInput, V2, Calculator):
    pass
