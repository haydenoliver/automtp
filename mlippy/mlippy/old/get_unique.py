from ase.db import connect
import numpy as np


db = connect('cfgs.db')

structs = [s for s in db.select()]

for struct_i in structs:
    for struct_j in structs:
        if not struct_i.id == struct_j.id:
            if struct_i.formula == struct_j.formula:
                if np.allclose(struct_i.positions, struct_j.positions):
                    print(struct_i.id, struct_i.formula, struct_i.positions)
                    print(struct_j.id, struct_j.formula, struct_j.positions,'\n\n')
