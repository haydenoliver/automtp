#!/bin/bash




iter=`cat iter_num`
mkdir logs/iter_$iter
mkdir logs/iter_$iter/train

echo "Beginning step 2 (calc-grade)..." | tee -a labBook
# module purge
# module load libfabric/1.5 mkl/2017 compiler_intel/2017 mpi/impi-2017_intel-2017

cd train;

mlp calc-grade curr.mtp train.cfg train.cfg temp.cfg --nbh-weight=0.0 --energy-weight=1.0;

# Copy to relax directory
cp state.mvs ../relax/state.mvs
cp curr.mtp ../relax/curr.mtp

# Copy to logs for recordkeeping
cp state.mvs ../logs/iter_$iter/train/state.mvs

cd .. 

echo "Finished step 2. Prepping relaxation step..." | tee -a labBook
./step_3_relax.sh
