#!/bin/bash
#
#
# This script, when run, will ALWAYS target the setup directory of this MTP
# It will concatenate all the config files from different prototype structures
# with identical cell sizes. These are generated beforehand by makeStr.py
#
#

# Set the source directory as the absolute path of the script
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd $DIR;
# echo working directory;
echo $PWD

# move to setup directory
cd ../../setup/;

# Join configs of equal cell sizes
for i in {1..10}; do
    # echo $i;
    echo;
    cat bcc/$i/vasp.{} hcp/$i/vasp.{} fcc/$i/vasp.{} > configs/simple/$i.cfg 2>/dev/null
done

# counter=2;
# cd configs;
# for struct in {2..10}; do
    # echo $struct;
    # for num in {1..$struct}; do

	# echo $num;
	# echo $counter;
	# if [ $num -le $struct ]; then
	    # cat simple/atom_$struct >> compound/cmp_$struct.cfg
	    
	    # echo ;
            
	# fi;
	# ((counter++))
# done;
# done


# END=10
# for ((i=1; i<=END; i++)); do
    # for struct in {1..$i}; do
	# cat simple/atom_$struct >> compount/cmp_$struct.cfg;
	# echo $struct;
    # done
# done
echo $PWD;
cd configs;
rm compound/*;

for i in $(seq 10 $END);
do
    for j in $(seq $i $END);
    do 
	echo $i $j
	cat simple/$j.cfg >> compound/$i.cfg;
    done;
done;