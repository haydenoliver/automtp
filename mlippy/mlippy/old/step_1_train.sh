#!/bin/bash

# Get iteration number
username=$USER
iter=`cat iter_num`


echo "==== Begin Training ====" | tee -a labBook

cd train; rm slurm*
cp train.cfg old_train.cfg
# Check if the node001 is available, if not, run locally.
node_state=`sinfo -h --nodes=node001 -o "%T"`
if [ "$node_state" == "idle" ]; # if the node is not currently allocated then execute the sbatch script
then
    # Launch the training script
    sbatch launch_train.sh
else
    mpiexec -n 8 mlp train curr.mtp train.cfg --max-iter=500 --trained-pot-name=curr.mtp --curr-pot-name=curr.mtp --stress-weight=5e-4 --force-weight=5e-3 > slurm-interactive.out
fi

while [ `squeue |grep $username |wc -l` -gt 0 ];
                             do squeue | echo `date`; sleep 10; done

tail -n 38 slurm* 
echo "Training Error Report" >> ../labBook
tail -n 40 slurm-*.out >> ../labBook
echo "End Step1 _____" | tee -a ../labBook

# Recordkeeping
cd ..
mkdir logs/iter_$iter/train/
cp train/curr.mtp logs/iter_$iter/train/
cp train/train.cfg logs/iter_$iter/train/
cp train/old_train.cfg logs/iter_$iter/train/old_train.cfg
cp train/slurm* logs/iter_$iter/train/


./step_2_calc_grade.sh
