import os
import subprocess as sp
##import ase

class MTP:
    """Parent class for the MLIP steps needed to build an MTP.
    To be used in conjuction with mlip_run.py.
    """

    def __init__(self, path, mlp_args='default'):
        # Get the instance of the Alloy system class to fill in training data
        self.path = path
        self.status = "incomplete"
        # self.iteration = iteration
        self.mlp_args = mlp_args

        # MTP.set_iter(iteration)
        self.log_path = os.path.join(self.path, 'log')


    def store_logs(self):
        steppath = os.path.join(self.path, self.step)
        shutil.copyfile(self.log_path)

    def set_status(self):
        # if no errors and the job is complete, set the status to Complete
        self.error_check()
        if self.errors == None:
            self.status = "Completed"
                        

    def convert_cfg(self, cfg_path, **kwargs):
        """
        Converts the structures contained in the mtp.cfg file to atoms objects to
        prepare for DFT calculations.

        default: mlip textual format
        bin:           mlip binary format
        vasp-outcar:   only as input; VASP versions 5.3.5 and 5.4.1 were tested
        vasp-poscar:   only as output. When writing multiple configurations,
            POSCAR0, POSCAR1, etc. are created
        lammps-datafile: only as output. Can be read by read_data from lammps.
            Multiple configurations are saved to several files.


        """
        pos_args = ["mlp", "convert-cfg"]

        opt_args = ['--input-format', # format of the input file
                    '-output-format', # format of the output file
                    '--append' # True/False           # opens output file in append mode
                    '--last', #:        ignores all configurations in the input 
                              #file except the last one (useful with relaxation)
                    '--fix-lattice']
                        #: creates an equivalent configuration by moving the atoms into
                        # the supercell (if they are outside) and tries to make the
                        # lattice as much orthogonal as possible and lower triangular.

        args = self._ParseOptArgs(pos_args, opt_args)
        sp.run(args, cwd=cfg_path)


    def Train(self, mtp_path, **opt_args):
        """
        The `Train` class is a subclass of the MTP class. It is used to run the training
        step for MTP and manage any inputs and outputs for the step.
        
        Mlip training parameters are set by default but can be changed at the user's
        discretion.
        
        
        """
        train_dir = os.path.join(mtp_path, 'train')
        pos_args = ["mlp",
                    "train",
                    "curr.mtp",
                    "train.cfg"
        ]
        opt_args_defaults = {"max-iter":500,
                             "trained-pot-name":"curr.mtp",
                             "curr-pot-name":"curr.mtp",
                             "stress-weight":5e-4,
                             "force-weight":5e-3
        }

        args = self._ParseOptArgs(pos_args, opt_args)
        sp.run(args, cwd=train_dir)


    def CalcGrade(self, mtp_path, **opt_args):
        calcgrade_dir = os.path.join(mtp_path, 'vasp')

        pos_args = ["mlp",
                    "calc-grade",
                    "curr.mtp",
                    "train.cfg",
                    "train.cfg",
                    "temp.cfg"]
        mlp_args_defaults = {"nbh-weight":0.0,
                             "energy-weight":1.0
        }


        args = self._ParseOptArgs(pos_args, opt_args)
        sp.run(args, cwd=calcgrade_dir)

    def Relax(self, mtp_path, **opt_args):
        """
        Options can be given in any order. Options include:
        --pressure=<num>: external pressure (in GPa)
        --iteration_limit=<num>: maximum number of iterations
        --min-dist=<num>: terminate relaxation if atoms come closer than <num>
        --force-tolerance=<num>: relaxes until forces are less than <num>(eV/Angstr.)
            Zero <num> disables atom relaxation (fixes atom fractional coordinates)
        --stress-tolerance=<num>: relaxes until stresses are smaller than <num>(GPa)
            Zero <num> disables lattice relaxation
        --max-step=<num>: Maximal allowed displacement of atoms and lattice vectors
            (in Angstroms)
        --min-step=<num>: Minimal displacement of atoms and lattice vectors (Angstr.)
            If all actual displacements are smaller then the relaxation stops.
        --bfgs-wolfe_c1
        --bfgs-wolfe_c2
        --cfg-filename=<str>: Read initial configurations from <str>
        --save-relaxed=<str>: Save the relaxed configurations to <str>
        --save-unrelaxed=<str>: If relaxation failed, save the configuration to <str>
        --log=<str>: Write relaxation log to <str>

        """
        relax_path = os.path.join(mtp_path, 'relax')
        pos_args = ["mlp",
                    "relax",
                    "mlip.ini"
        ]

        if len(opt_args) == 0:        
            opt_args = {
                "force-tolerance":1e-3,
                "stress-tolerance":1e-2,
                "max-step":0.03,
                "cfg-filename":"to-relax.cfg",
                "save-relaxed":"relaxed.cfgsr",
                "save-unrelaxed":"unrelaxed.cfg"
            }

        args = self._ParseOptArgs(pos_args, opt_args)
        print(args)
        sp.run(args, cwd=relax_path)


    def SelectAdd(self, mtp_path, **opt_args):
        select_path = os.path.join(mtp_path, 'select')

        pos_args = ["mlp",
                    "select-add",
                    "curr.mtp",
                    "train.cfg",
                    "preselected.cfg",
                    "selected.cfg"
        ]
        opt_args_defaults = { "select-threshold":2.0,
                              "nbh-weight":0.0,
                              "energy-weight":1.0,
                              "mvs-filename":"state.mvs",
                              "selected-filename":"active_set.cfg",
                              "select-limit":200
        }

        args = self._ParseOptArgs(pos_args, opt_args)
        sp.run(args, cwd=select_path)


    def _ParseOptArgs(self, *args_in):
        """
        Concatenates positional and optional arguments into a single list
        interpretable by subprocess.run()
        
        Args:
            args_in (tuple):
                - positional arguments for MLP
                - optional arguments for MLP (needs to have '--' in front of the argument)
        """   

        args = []
        for arglist in args_in:
            if type(arglist) is list:
                for item in arglist:
                    if type(item) is list:
                        for sub_item in item:
                            args.append(str(sub_item))
                    else:
                        args.append(item)
            elif type(arglist) is dict:
                for key in arglist:
                    args.append('--' + key)
                    args.append(str(arglist[key]))
            else:
                args.append(item)
                
        return args
            
