import subprocess as sp

train_opt_args = {"max-iter":500,
                  "trained-pot-name":"curr.mtp",
                  "curr-pot-name":"curr.mtp",
                  "stress-weight":5e-4,
                  "force-weight":5e-3
}

train_pos_args = ["mlp",
                  "train",
                  "curr.mtp",
                  "train.cfg"
                  ]

train_opt_args_tmp = []
for item in train_opt_args.items():
    train_opt_args_tmp.append('='.join([str(i) for i in item]))
opt_args = ['--' + s for s in opt_args_tmp]

train_args = train_pos_args + opt_args

with open('train.report', 'wb') as file_out:
    sp.run(train_args,
           stderr=file_out,
           stdout=file_out
           )
