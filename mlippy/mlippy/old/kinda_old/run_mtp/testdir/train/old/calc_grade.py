import subprocess as sp


calc_grade_pos_args = ["mlp",
                       "calc-grade",
                       "curr.mtp",
                       "train.cfg",
                       "train.cfg",
                       "temp.cfg"]
                       

calc_grade_opt_args = {"nbh-weight":0.0,
                   "energy-weight":1.0
                   }


opt_args_tmp = []
for item in calc_grade_opt_args.items():
    opt_args_tmp.append('='.join([str(i) for i in item]))
opt_args = ['--' + s for s in opt_args_tmp]



calc_grade_args = calc_grade_pos_args + opt_args

print(calc_grade_args)

with open('calc_grade.report', 'wb') as file_out:
    sp.run(calc_grade_args,
       stderr=sp.PIPE,
       stdout=file_out)
