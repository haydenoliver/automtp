import multiprocessing as mp
import subprocess as sp
import os

from ase import Atoms, Atom
from ase.db import connect

from vasp_gr import Vasp2





def exec_vasp(db_row, calc):
    struct_dir = os.path.join(os.getcwd(), "dir_POSCAR-" + str(db_row.id))
    try:
        os.mkdir(struct_dir)
    except:
        pass


    # Set calculator directory
    calc.directory =  struct_dir

    # Add calculator to the Atoms object
    struct = db_row.toatoms()
    struct.set_calculator(calc)

    # Calculate energy
    energy = struct.get_potential_energy()
    

def db_connect(filename="cfgs.db"):
    db = connect('cfgs.db')
    return db



if __name__ == "__main__":
    
    # Set up the connection to the database created from the MTP Selection step
    db = db_connect()

    # Set the ASE atoms calculator with the custom vasp object

    calc = Vasp2(prec='Accurate',
             xc='PBE',
             lreal=False,
             command="vasp54s",
             )

    ## print("Number of processors:", mp.cpu_count())

    # Create a list of processes based on the number of entries in the config database
    processes = [mp.Process(target=exec_vasp, args=(row, calc)) for row in db.select()]

    # Run processes
    for p in processes:
        p.start()

    # WAIT on the processes to finish before
    for job in jobs:
        job.join()
