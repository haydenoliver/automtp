#!/bin/bash

ROOT=$PWD

rm mlp.log out.cfg;
for i in dir_POSCAR-*; do
    echo $i;
    cd $ROOT/$i;
    mlp convert-cfg OUTCAR outcar.cfg --input-format=vasp-outcar --append;
    ../__mlip_post.pl;
    cat _outcar.cfg >> ../out.cfg;
done >>mlp.log 2>>mlp.log;

cd $ROOT
mlp filter-nonconv out.cfg
cp ./out.cfg ./converged_poscars.cfg
