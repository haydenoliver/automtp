import sys
import os

"""This file generates a list of all the derivative structures of the prototype cells.
In this case `bcc, fcc, hcp, sc`. By default the file outputs the parts of 


Args (sys.argv format):
	sys.argv[0] (str): name of the file
	sys.argv[1] (str): structure prototype
	sys.argv[2] (str): minimum number of atoms in the cell
	sys.argv[3] (str): maximimum number of atoms in the cell 



Examples:
	`python build_structs.py fcc 1 4`
    This example builds directories of the fcc-prototype structure containing an input file for makeStr.py to read
This contains the structures of 1-4 atoms in the unit cell (approximately 100 structures total)



  
"""





# cell sizes for FCC, BCC, SC
str_min = 1
str_max = 10

# cell sizes for HCP (two lattice points, so n/2 is the number of atoms)
if sys.argv[1].lower() == "hcp":
	str_min = 1
	str_max = 5
	hcp_tf = True
else:
	hcp_tf = False



#build structures for cubic systems

#with open ("prototypes/struct_enum." + sys.argv[1]) as f:
#	struct = f.read()
	
min_size = 1

for cell_size in range(str_min, str_max + 1):
	with open ("prototypes/struct_enum." + sys.argv[1]) as f:
		struct = f.read()
	
	max_size = cell_size # from loop condition
	print(min_size, max_size)
	struct = struct.replace("cell_size_min", str(min_size)).replace("cell_size_max", str(max_size))
	

	# Set naming of the directories. HCP name will be 2x cell size 
	if hcp_tf == True:
		struct_path = sys.argv[1] + "/" + str(2 * cell_size)
	else:
		struct_path = sys.argv[1] + "/" + str(cell_size)
	
	if not os.path.exists(struct_path):
		os.mkdir(struct_path)

	with open (struct_path + "/struct_enum.in", 'w') as fw:
		fw.write(struct)
	

	enum_cmd = "cd " + struct_path + " && enum.x"
	os.system(enum_cmd)
	
	makeStr_cmd = "cd " + struct_path + " && python	../../makeStr.py all -species Co Zn -config t"
	os.system(makeStr_cmd)


	min_size = max_size
