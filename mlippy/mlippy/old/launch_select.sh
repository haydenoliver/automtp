#!/bin/bash

#SBATCH --time=1:00:00   # walltime
#SBATCH --ntasks=2   # number of processor cores (i.e. tasks)
#SBATCH --nodes=1   # number of nodes
#SBATCH --mem-per-cpu=2GB   # memory per CPU core
#SBATCH -J "Relax"   # job name

# Set the max number of threads to use for programs using OpenMP. Should be <= ppn. Does nothing if the program doesn't use OpenMP.
# export OMP_NUM_THREADS=1

# Get the path to the executable; should be on user's path after the modules have been loaded.
# module purge
# module load mpi/openmpi-1.8.5_intel-15.0.2 
# module switch compiler_intel/15.0.2 compiler_intel/2017

mpirun -n 2 mlp select-add curr.mtp train.cfg preselected.cfg selected.cfg --select-threshold=2.0 --nbh-weight=0.0 --energy-weight=1.0 --mvs-filename=state.mvs --selected-filename=active_set.cfg --select-limit=200


