#!/bin/bash

#SBATCH --time=2:00:00   # walltime
#SBATCH --ntasks=4   # number of processor cores (i.e. tasks)
#SBATCH --nodes=1   # number of nodes
#SBATCH --mem-per-cpu=512MB   # memory per CPU core
#SBATCH -J "CoZn-relax"   # job name

# Set the max number of threads to use for programs using OpenMP. Should be <= ppn. Does nothing if the program doesn't use OpenMP.
# export OMP_NUM_THREADS=

# Get the path to the executable; should be on user's path after the modules have been loaded.
# module purge
# module load libfabric/1.5 mkl/2017 compiler_intel/2017 mpi/impi-2017_intel-2017

# This can be run multithreaded as well
# `mpirun -n 4` goes before the mlp command
mpirun -n 4 mlp relax mlip.ini --force-tolerance=1e-3 --stress-tolerance=1e-2 --max-step=0.03 --cfg-filename=to-relax.cfg --save-relaxed=relaxed.cfgsr

