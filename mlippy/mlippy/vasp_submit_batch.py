#!/apps/python/3.7.3/gcc-8.3.0_sqlite3/bin/python

#SBATCH --time=8:00:00   # walltime
#SBATCH --ntasks=8  # number of processor cores (i.e. tasks)
#SBATCH --mem-per-cpu=2GB   # total memory
#SBATCH -J "VaspCalcs"
#SBATCH -p "physics"


import os
from ase.db import connect
from ase.calculators.vasp import Vasp2

from mlippy.VaspRun import ExecVasp

""" 
    This script is intended to be submitted by the SBATCH job manager. It is used to launch the VASP calculations
for all of the calculations that need to be completed to train the MTP.

"""

os.environ["VASP_SCRIPT"]="/fslhome/holiver2/automtp/mlippy/src/run_vasp.py"
os.environ["VASP_PP_PATH"]="/fslhome/holiver2/fsl_groups/fslg_msg_vasp/"
os.environ["VASP_COMMAND"]="srun vasp_ncl"


### set up database
db = connect('cfgs.db')


task_id = int(os.environ["SLURM_ARRAY_TASK_ID"])


print("TASK_ID:", task_id)
print(db[task_id])

#print('test7')

ExecVasp(task_id, db)
