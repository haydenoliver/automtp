#!/bin/bash

#SBATCH --time=8:00:00   # walltime
#SBATCH --ntasks=8  # number of processor cores (i.e. tasks)
#SBATCH --mem-per-cpu=2GB   # total memory
#SBATCH -J "VASP_run"
##SBATCH -C "knl"
##SBATCH --qos='msg'
#SBATCH -p "physics"

module purge
module load python/3.7 gcc/8 #openmpi/3.1

#export OMP_NUM_THREADS=$SLURM_NTASKS

export VASP_SCRIPT="~/automtp/mlippy/mlippy/run_vasp.py"
export VASP_PP_PATH="/fslhome/$USER/fsl_groups/fslg_msg_vasp/"
export VASP_COMMAND="srun vasp_ncl"
#export STRUCT_DB_ID=$1


python ~/automtp/mlippy/mlippy/VaspRun.py --run --struct_id $SLURM_ARRAY_TASK_ID
#echo $SLURM_ARRAY_TASK_ID
