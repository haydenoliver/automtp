#!/bin/bash


module purge
module load gcc/8 openmpi/4.0

iter=`cat iter_num`
username=$USER

echo "Beginning relaxtion step..." | tee -a labBook

cd relax;

# Clean up old files
rm slurm*.out
rm selected*
rm preselected*

# Launch the training script
slurm_submit_string=`sbatch launch_relax.sh`
set -- $slurm_submit_string
job_id=$4

while [ `squeue |grep -c $job_id` -gt 0 ];
do
    echo "Waiting on job: $job_id"; sleep 10;
done

cat selected.cfg_* >preselected.cfg; rm selected.cfg_*
cat relaxed.cfgsr* >relaxed.cfg; rm relaxed.cfgsr*

fitted=`grep BEGIN relaxed.cfg | wc -l`
echo "Number of fitted systems: $fitted" >> ../labBook

# recordkeeping
cd ../
mkdir logs/iter_$iter/relax
cp relax/slurm* logs/iter_$iter/relax/
cp relax/to-relax.cfg logs/iter_$iter/relax/
cp relax/preselected.cfg logs/iter_$iter/relax/
cp relax/relaxed.cfg logs/iter_$iter/relax


# We're going to count the number of structures in the to-relax.cfg and see if
# we were able to relax all of them by comparing agains the number in the
# relaxed.cfg file. If they are equal the loop ends, if not, they will call
# step 4 and keep going in a cycle until they reach the same number.
to_relax=`grep BEGIN relax/to-relax.cfg | wc -l`
relaxed=`grep BEGIN relax/relaxed.cfg | wc -l`
echo "To_relax = $to_relax"
echo "relaxed = $relaxed"
if [ $to_relax != $relaxed ]; then
    echo "Not the same"
    echo "$relaxed of $to_relax structures relaxed: " | tee -a labBook
    echo "Not all structures have relaxed. Selecting new data..." | tee -a labBook

    ./step_4_select_add.sh
else
    # echo "The same"
    echo "All structures have relaxed. Increasing number of structures in to-relax.cfg." | tee -a labBook
    cellsize=`cat cell_size`
    echo "cell size = $((cellsize++))"
    echo $cellsize > cell_size

    echo "Copying $cellsize.cfg onto relax/to-relax.cfg" | tee -a labBook
    
    cp setup/configs/compound/$cel# lsize.cfg relax/to-relax.cfg
    
    echo "iter = $((iter++))"
    echo  $iter > iter_num
    echo "Starting new iteration. Next iteration is: $iter" | tee -a labBook
    echo "Restarting relax step with larger test set..." 


    # mkdir logs/iter_$iter/relax


    if [ $cellsize > 10 ]; then
	exit;
    else 
	mkdir logs/iter_$iter/
	# ./step_3_relax.sh
    fi
    
    
fi


