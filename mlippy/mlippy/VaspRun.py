import argparse
import mpi4py
import multiprocessing as mp
import subprocess as sp
import sys
import os
import shutil

from ase import Atoms, Atom
from ase.db import connect

from calculators.vasp_gr import Vasp2



def get_args():
    parser = argparse.ArgumentParser(description='Setup the slurm submission and execute VASP calculations')
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('--setup', action='store_true',
                       help='Setup the runs to be calculated')
    group.add_argument('--run', action='store_true',
                       help="Run the calculations")
    parser.add_argument('--struct_id', type=int,
                        help='Database ID for structure to be calculated')
    
    args = parser.parse_args()

    print(args)
    return args

setups_vasp_defaults = {'Li': '_sv',
                        'Co': '_pv',
                        'Ni': '_pv',
                        'Fe': '_pv',
                        'Na': '_pv',
                        'K': '_sv',
                        'Ca': '_sv',
                        'Sc': '_pv',
                        'Ti': '_sv',
                        'V': '_sv',
                        'Cr': '_pv',
                        'Mn': '_pv',
                        'Ga': '_d',
                        'Ge': '_d',
                        'Rb': '_sv',
                        'Sr': '_sv',
                        'Y': '_sv',
                        'Zr': '_sv',
                        'Nb': '_sv',
                        'Mo': '_sv',
                        'Tc': '_pv',
                        'Ru': '_pv',
                        'Rh': '_pv',
                        'In': '_d',
                        'Sn': '_d',
                        'Cs': '_sv',
                        'Ba': '_sv',
                        'Pr': '_3',
                        'Nd': '_3',
                        'Pm': '_3',
                        'Sm': '_3',
                        'Eu': '_2',
                        'Gd': '_3',
                        'Tb': '_3',
                        'Dy': '_3',
                        'Ho': '_3',
                        'Er':'_3',
                        'Tm':'_3',
                        'Yb':'_2',
                        'Lu':'_3',
                        'Hf': '_sv',
                        'Ta': '_pv',
                        'W': '_pv',
                        'Tl': '_d',
                        'Pb': '_d',
                        'Bi':'_d',
                        'Po': '_d',
                        'At': '_d',
                        'Fr':'_sv',
                        'Ra':'_sv'
}


def ExecVasp(struct_id, db):

    struct = db.get_atoms(id=struct_id)
    prefix =  "dir_POSCAR-" + str(struct_id)
    struct_dir = os.path.join(os.getcwd(), prefix)
    # Check to see if the calculation has been started in the past

    encut = calc_ENCUT(struct)

    calc = Vasp2(prec='Accurate',
                 xc='PBE',
                 directory=struct_dir,
                 icharg=1,
                 ibrion=1,
                 lreal='auto',
                 encut=encut,
                 ismear=2,
                 sigma=.005,
                 isif=3,
                 setups=setups_vasp_defaults,
                 restart=True,
                 ignore_bad_restart_file=True,
#                      LWAVE=False
)



    struct.set_calculator(calc)
    print(struct_dir, "TESTING")

    # Calculate values using Vasp
    energy = struct.get_potential_energy()
    magmom = struct.get_magnetic_moment()
   
    db.update(id=int(struct_id), atoms=struct)




#def db_connect(db_name="cfgs.db"):
#    """ Creates a ase.db connection to the sql database generated
#    from the selection step.
#
#    Args:
#      db_name (str): Filename of database to be read in
#
#    Returns:
#      db (ase.db.connect): SQL database connection
#    """
#    db = connect(db_name)
#    return db



def calc_ENCUT(struct):
    """
    Reads the species in the Atoms object, reads the ENMAX value from each POTCAR associated with the species, and calculates the ENCUT argument for vasp.

    ENCUT = 1.3 * (largest ENMAX)


    Args:
    struct (ASE.Atoms): 
    """
    VASP_PP_PATH = "/fslhome/holiver2/fsl_groups/fslg_msg_vasp/"  # hardcoded. Needs fixing.
#    PP_PATH = os.environ.get('VASP_PP_PATH')

    encut = []    
    for species in struct.get_chemical_symbols():
#        print(species, setups_vasp_defaults[species])
        potcar = species + setups_vasp_defaults[species]
        pot_path = os.path.join(VASP_PP_PATH, "potpaw_PBE", potcar, "POTCAR")
        with open(pot_path, 'r') as pot:
            head = [next(pot) for i in range(50)]
        
        for line in head:
            if "ENMAX" in line:
                enmax = float(line.split()[2].replace(';', ''))
                ENCUT = int(1.3*enmax)
                encut.append(ENCUT)

    max_encut = max(encut)
 #   print(encut, max_encut)
    return max_encut


def setup_slurm_array(db, db_path='.'):
    """
    Setup for the slurm array. The usage of a slurm array allows the submission of large numbers of jobs while setting
    a limit for the number of jobs that can run concurrently. This helps to alleviate the pressure on limited computing resources
    and makes it easier for others to share those same resources simultaneously.
    
    Args:
    db (ASE.db.database): Database object containing data for each structure in training set. 
    db_path (path, str): Relative or absolute path of the database 

    """

    job_array = []

    for struct in db.select():
        struct_path = os.path.join(db_path, "dir_POSCAR-" + str(struct.id))
        try:
            struct.energy
        except AttributeError:
            if os.path.isfile(os.path.join(struct_path, "OUTCAR")):
                restart = True
            else:
                restart = False
                
            job_array.append(str(struct.id))

    arg_job_array = ','.join(job_array)
#    print(arg_job_array)
    return arg_job_array




def main():
    db = connect('cfgs.db')
    #   if args.setup:
    jobs = setup_slurm_array(db, os.getcwd())
    print(jobs)

    max_concurrent_jobs = 20 
    
    os.system("sbatch --array " + jobs + "%" + str(max_concurrent_jobs) + " ~/automtp/mlippy/mlippy/vasp_submit_batch.py")
    print('Calculating training data')
#    elif args.run:
#        print("RUNNING CALCULATIONS")
#        print(args.struct_id, "RUNNING")
#        exec_vasp(args.struct_id, db)

    

if __name__ == "__main__":
#    args = get_args()
    main()
