import cmd
import string, sys
import ruamel.yaml as yaml


class CLI(cmd.Cmd):

    def __init__(self):
        cmd.Cmd.__init__(self)
        self.prompt = '> '

    def do_hello(self, arg):
        print "hello again", arg, "!"

    def help_hello(self):
        print "syntax: hello [message]",
        print "-- prints a hello message"

    def do_quit(self, arg):
        sys.exit(1)

    def help_quit(self):
        print "syntax: quit",
        print "-- terminates the application"


    def do_given_name(self, arg):
        print("Building atomic structures library")

        
        
        inp = """\
        # example
    name:
        # details
    family: Smith   # very common
    given: Alice    # one of the siblings
        """
        
        yaml = YAML()
        code = yaml.safe_load(inp)
        code['name']['given'] = "Bob" #raw_input("What is the given name?")

        yaml.dump(code, sys.stdout)

        
        
    def help_given_name(self):
        print(build_struct_help)



    def do_show_configs(self, arg):
        with open('config_2.yaml') as fp:
            data = yaml.safe_load(fp)
        # for elem in data:
            # print(elem)
        print(data)

        
    def help_show_configs(self):
        print("Shows all the current config settings for this job")
    # shortcuts



    
    do_q = do_quit

#
# try it out

# Help messages

build_struct_help = (
    """
    (help: build_struct) 
    This function creates the input files for the enumeration library.
    The input file options for `struct_enum.in` are  """
)



cli = CLI()
cli.cmdloop()
