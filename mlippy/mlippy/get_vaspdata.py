import os
import pandas as pd

dirs = os.listdir(".")
dir_poscar_list = []

for item in dirs:
    if "dir_POSCAR" in item:
        dir_poscar_list.append(item)

with open("../iter_num") as i:
    iter_num = int(i.read())

sorted_poscars = sorted(dir_poscar_list)


all_runs = []
for poscar_dir in sorted_poscars:
    
    # Get directory number for better sorting
    dir_num = int(poscar_dir.split("-")[-1])

    
    
    # Get directory path and contents
    runpath = os.path.join(os.getcwd(), poscar_dir)
    subdirs = os.listdir(runpath)
    print(runpath)

    # Get the slurm job number
   # slurm_out = [s for s in subdirs if "slurm-" in s][0].replace('.','-')
   # job_num = int(slurm_out.split("-")[1])

    
    # Get the atomic species list
    with open (os.path.join(runpath, "__mlip_species")) as mlip:
        species_old = mlip.read()
    species = species_old.replace('0', 'Co').replace('1', 'Zn').split()

    # Get the number of atoms
    with open (os.path.join(runpath, "POSCAR")) as poscar:
        plines = poscar.readlines()
	
    n_atoms = sum(map(int, plines[5].split()))

    # Get the outcar 
    with open(os.path.join(runpath, "OUTCAR")) as outcar:
        flines = outcar.readlines()

    # Get the oszicar
    with open(os.path.join(runpath, "OSZICAR")) as oszi:
        oszilines = oszi.readlines()

    # Extract run data from outcar
    cputime = None
    totaltime = None

    # Get the number of cores used
    

    for line in flines:
        if 'total cores' in line:
            ncores = int(line.split()[2])
            # print(ncores)
            # print(type(ncores))
    
    for line in reversed(flines):
        if "Total CPU time used (sec):" in line:
            cputime = float(line.split()[-1])
            totaltime = cputime * float(ncores)
            break
        elif "Maximum memory used (kb):" in line:
            memory = float(line.split()[-1])
    
    # Extract energy data from oszicar
    try:
        if "F=" in oszilines[-1]:
            etot = float(oszilines[-1].split()[2])
            status = True
    except:
        print("Run failed to complete: setting completion status to 'False'")
        status = False

    # Build dict containing all relevent information
    vasp_run = {"dir_num":dir_num,
#                "job_num":job_num,
		"n_atoms":n_atoms,
                "CPUtime":cputime,
                "n_cores":ncores,
                "total_cpu_time":totaltime,
                "memory":memory,
                "energy":etot,
                "species":species,
                "iter_num":iter_num,
                "job_status":status 
                }
    all_runs.append(vasp_run)
    

for job in all_runs:
    print(job)

df = pd.DataFrame(all_runs)
print(df.sort_values(by="dir_num"))
df.to_pickle("./data.pkl")
df.to_csv("data.csv", index=False)
