#!/bin/bash

# Get iteration number

iter=`cat iter_num`
root=`pwd` 
alloy=${PWD##*/}

# Copy files from vasp runs
# cp vasp/selected.cfg logs/iter_$iter/vasp/

#cp vasp/mlp.log logs/iter_$iter/vasp/
cp vasp/slurm*.out logs/iter_$iter/vasp/
cp vasp/cfgs.db logs/iter_$iter/vasp/

echo "Finished with DFT. Beginning training now..." | tee -a labBook
echo "" | tee -a labBook


echo "==== Begin Training ====" | tee -a labBook


module purge
module load python/3.7

cd vasp 
echo '0' > write_db.txt
python ~/automtp/mlippy/mlippy/get_training_set.py $root/../$alloy.yaml
cd $root

cp vasp/_train.cfg train/train.cfg

cd train; rm slurm*
cp train.cfg old_train.cfg

# Run the training step
slurm_submit_string=`sbatch launch_train.sh`
set -- $slurm_submit_string
job_id=$4

while [ `squeue | grep -c $job_id` -gt 0 ];
                             do squeue | echo `date`; sleep 10; done

tail -n 38 slurm* 
echo "Training Error Report" >> ../labBook
tail -n 40 slurm-*.out >> ../labBook
echo "End Step1 _____" | tee -a ../labBook

# Recordkeeping
cd ..
mkdir logs/iter_$iter/train/
cp train/curr.mtp logs/iter_$iter/train/
cp train/train.cfg logs/iter_$iter/train/
cp train/old_train.cfg logs/iter_$iter/train/old_train.cfg
cp train/slurm* logs/iter_$iter/train/


./step_2_calc_grade.sh
