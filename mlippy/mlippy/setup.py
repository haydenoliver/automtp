import yaml
import os
import sys
import subprocess

""" This is the docstring for the module"""


def _get_args():
    import argparse
    parser = argparse.ArgumentParser(description='Choose a filepath for the structure to work on')

    parser.add_argument('input', #'-i',
                        #         action='store_const',
                        #                    const='system.yaml',
                        default='system.yaml',
                        # required=True,
                        help='Path to the system yaml file you want to setup and prepare for MTP'
                        )
    # parser.add_argument('--concentration ', '-c',
                        # nargs='+',
                        # metavar='',
                        # help=("""Concentration ranges for each atom. List by atom type alphabetically.
                        # Listed as specified in the Enumlib documentation.
                        # ex. (a structure of 3 atom types, 0, 1, 2)
                           # -c 1 1 2  
                        # """
    # )
    parser_structs = parser.add_mutually_exclusive_group()

    parser_structs.add_argument('--cell_size_range', '-cs',
                                default=[1, 10],
                                nargs=2,
                                type=int,
                                metavar='',
                                help='Determines the size range of the number of atoms in the unit cell for the enumeration. Default is the full range of structures')

    parser_structs.add_argument('--all_cells', '-a',
                                # action='set_true',
                                # default=True,
                                # nargs=0,
                                metavar='',
                                help="Builds all config files increasing in size, each including the previous set"
    )

    args = parser.parse_args()
    # yml_path = os.path.abspath(args.input)

    # print(type(args.cell_size_range), args.cell_size_range)

    # _cell_size = args
    # quit()
    return args   # yml_path, _cell_size

# yaml_path = _get_args()


def get_yaml(_args=None):
    if _args is None:
        yaml_path = 'system.yaml'
    else:
        yaml_path = os.path.abspath(args.input)

    with open(yaml_path, 'r') as f:
        yml = yaml.safe_load(f)
    # quit()
    # root = yml['path']['ROOT']
    root = yml['ROOT']
    # setup_source = os.path.join(root, yml['path']['setup'])
    system_target = root  # os.path.join(root, yml['path']['struct_path'])
    setup_target = os.path.join(root, yml['path']['struct_path'], yml['output']['setup'])
    
    # Add scripts path to environment path to call script executables
    # scripts_path = os.path.join(root, 'bin')
    # print(scripts_path)

    # os.environ['PATH'] += os.pathsep + scripts_path
    # my_env = os.environ.copy()

    print("Working in :", system_target)
    if not os.path.isdir(setup_target):
        os.makedirs(setup_target)
    return yml  # , my_env


def write_struct_enum(yml, _args=None, cell_size_max=None):
    # from the parent yaml

    if _args is not None:
        if _args.input:
            yml_path = os.path.abspath(_args.input)
            with open(yml_path, 'r') as f:
                yml = yaml.safe_load(f)
        if _args.cell_size_range:
            # cell_sizes = '  '.join([str(x) for x in _args.cell_size_range])
            cell_sizes = _args.cell_size_range
    elif cell_size_max is not None:
        cell_sizes = [1, cell_size_max]
    else:
        cell_sizes = [1, 10]

    root = yml['ROOT']

    # struct_path = os.path.join(
        # root,
        # yml['path']['struct_path']
    # )
    struct_path = root

    setup_target = os.path.join(root, yml['path']['struct_path'], yml['output']['setup'])
    # setup_target = os.path.join(struct_path, yml['output']['setup'])

    # quit()
    # setup_source = os.path.join(yml['path']['ROOT'], yml['path']['setup'])
    # print(list(yml['species'].keys()))

    num_species = len(yml['species'].keys())
    if num_species == 2:
        system_type = 'binary'
    elif num_species == 3:
        system_type = 'ternary'
    else:
        system_type = 'other'

    # open yaml template and build struct_enum.in files
    # yml_struct = yml['enumeration']
    # print(
    try:
        enum_yaml_path = os.path.join(root, 'mtp', yml['_enumeration_yaml_path']['filename'])
        yml_struct = yaml.safe_load(open('enum.yaml'))
    except:
        raise FileNotFoundError("'enum.yaml' file not found")
    
    for struct in list(yml_struct['prototype'].keys()):
        out_dir = os.path.join(setup_target, struct)
        # print(out_dir)

        if not os.path.isdir(out_dir):
            os.makedirs(out_dir)
        out_path = os.path.join(out_dir, 'struct_enum.in')  # + struct)

        with open(out_path, 'w') as yw:
            print(struct, file=yw)
            print(yml_struct['type'], file=yw)

            for x in yml_struct['prototype'][struct]['vectors']:
                print(x, file=yw)

            print(str(yml_struct['system'][system_type]) + " -nary case", file=yw)

            # multilattice points and restrictions
            lattice_points = yml_struct['prototype'][struct]['lattice_points']
            print('  ', len(lattice_points.keys()), '# number of points in multilattice', file=yw)
            for point in lattice_points:
                # print(yml_struct['prototype'][struct]['lattice_points'][point]['cart'])
                cart = yml_struct['prototype'][struct]['lattice_points'][point]['cart']
                restr = yml_struct['prototype'][struct]['lattice_points'][point]['restr']
                print(' ', cart, restr, ' # d-vector', file=yw)

            # Set cell size for struct_enum.in
            # print(cell_sizes, type(cell_sizes))
            if struct == 'hcp':
                from math import ceil
                cell_sizes_ceiling = [ceil(x/2) for x in cell_sizes]
                cell_sizes_hcp = ' '.join([str(x) for x in cell_sizes_ceiling])
                print(cell_sizes_hcp, ' # starting and ending cell_sizes', file=yw)
            else:
                cell_sizes_fcc_bcc = '  '.join([str(x) for x in cell_sizes])
                print(cell_sizes_fcc_bcc, ' # starting and ending cell_sizes', file=yw)

            print(yml_struct['finite_precision'], ' # finite precision parameter',  file=yw)
            print(yml_struct['labelings'], ' list of labelings', file=yw)

            if yml_struct['concentrations'] != 'None':
                print('# Concentrations for each atom', file=yw)
                for key in sorted(list(yml_struct['concentrations'].keys())):
                    print(yml_struct['concentrations'][key], file=yw)


def _edit_struct_enum_in():
    pass


def setup_makeStr(yml, _args=None, cell_size_max=None):
    from multiprocessing import cpu_count, Process  # , Pool

    enum_yaml_path = os.path.join(yml['ROOT'], 'mtp',
                                  # yml['_enumeration_yaml_path']['struct_path'],
                                  yml['_enumeration_yaml_path']['filename']
    )
    enum_yml = yaml.safe_load(open(enum_yaml_path))
    protos = list(enum_yml['prototype'].keys())

    jobs = []
    # set paths
    root = yml['ROOT']
    # struct_path = os.path.join(
        # root,
        # yml['path']['struct_path']
    # )
    struct_path = root
    setup_target = os.path.join(root, yml['path']['struct_path'], yml['output']['setup'])
    # setup_target = os.path.join(struct_path, yml['output']['setup'])
    # print(setup_target, typ(esetup_target))
    # quit()

    for proto in protos:
        # print(proto)

        enumerated_struct_path = os.path.join(setup_target, proto)
        print(enumerated_struct_path, 'enumerated_struct_path')
        # quit()
        if cpu_count() >= 3:
            p = Process(target=run_enumx_makeStr, args=(yml, enumerated_struct_path))

            print('Running in parallel')
            jobs.append(p)
            p.start()

        else:
            # print('Running serially')
            run_enumx_makeStr(yml, enum_path=enumerated_struct_path)

    # WAIT on the processes to finish before
    for job in jobs:
        job.join()


    # cell_sizes = a
    # setup args for config concatentation
    if cell_size_max is not None:
        cell_sizes_all = [1, cell_size_max]
        cell_sizes = '_'.join([str(x) for x in cell_sizes_all])
    elif _args is not None:
        if _args.cell_size_range:
            cell_sizes = '_'.join([str(x) for x in _args.cell_size_range])
        else:
            cell_sizes = '_'.join(
                str(x) for x in (yml['enumeration']['prototype']['fcc']['cell_sizes']))

    vasp_cfg_path = os.path.join(setup_target, cell_sizes + '.cfg')
    with open(vasp_cfg_path, 'w'):
        pass
    # print(vasp_cfg_path)
    cat_args = ['cat']
    cat_structs = [os.path.join(proto, 'vasp.cfg') for proto in protos]
    cat_args = cat_args + cat_structs
    # print(cat_args)
    with open(vasp_cfg_path, 'wb') as f:
        subprocess.run(cat_args, cwd=setup_target, stdout=f)

    cfg_count = subprocess.run(['grep', '-c', 'BEGIN', vasp_cfg_path],
                               cwd=setup_target,
                               stdout=subprocess.PIPE)
    print("Number of structures:", cfg_count.stdout.decode('utf-8'))
    print("DONE")
    return vas_cfg_path

def run_enumx_makeStr(yml, enum_path):
    """ This is the dictionary that makeStr.py is using as input. Here we
    will create a key:value mapping for each argument that makeStr.run()
    requires. These will be taken from the "system.yaml" file in the
    system root directory.

    makeStr.run() example input dictionary

    dict = {
    'examples': False,
    'verbose': False,
    'action': 'print',
    'debug': False,
    'structures': ['all'],
    'displace': 0.0,
    'input': 'struct_enum.out',
    'mink': 't',
    'species': ['Al', 'Co', 'W'],
    'species_mapping': [0, 1, 2],
    'outfile': 'vasp.{}',
    'rattle': 0.0,
    'config': 'f',
    'remove_zeros': 'f'
    }
    """
    import makeStr

    args = {}
    species_data = {}

    # extract the available parameters from the system.yaml
    species_data['species'] = list(yml['species'].keys())
    species_data['species_mapping'] = list(yml['species'].values())

    # Set input and output paths
    paths = {}
    paths['input'] = os.path.join(enum_path, 'struct_enum.out')
    paths['outfile'] = os.path.join(enum_path, 'vasp.cfg')

    # set default parameters for makeStr. Taken from the Yaml file
    from_yaml = yml['for_makeStr']

    # join the two dicts key-wise
    args = {**species_data, **from_yaml, **paths}
    # args['species'] = list(args['species'].keys())
    # args['species'] = list(args['species'].values())
    # execute the enumeration using enum.x

    import subprocess as sp

    # print(enum_path, type(enum_path))
    # quit()
    print(enum_path)
    # quit()
    with open(os.path.join(enum_path,  'enum.out'), 'ab') as f:
        # Cleanup before enum.x
        my_env = os.environ.copy()

        sp.run(['rm',
                'enum.out',
                'vasp.cfg'],
               cwd=enum_path,
               stderr=sp.PIPE,
               stdout=sp.PIPE,
               env=my_env
               )

        # Run enum.x
        # print('Running: enum.x')
        sp.run(['enum.x'], cwd=enum_path, stderr=f, stdout=f, env=my_env)

        # run makeStr.py
        makeStr.run(args)


if __name__ == '__main__':
    args = _get_args()
    # print(args)
    yml = get_yaml(_args=args)
    # print(yml['path'])

    if args.all_cells:
        for i in range(10):
            write_struct_enum(yml, cell_size_max=(i+1))
            catpath = setup_makeStr(yml, cell_size_max=(i+1))
    else:
        write_struct_enum(yml, _args=args)
        catpath = setup_makeStr(yml, _args=args)

    # Fix the conf_id tags for each struct
    import fix_conf_id
    fix_conf_id.fix_conf_id(catpath)
