import sys
from glob import glob
import os

from ase.db import connect

from mlippy.mlip import set_mapping, write_to_cfg, convert_to_db

"""
This script runs MLIP.py to convert the values stored in the ASE database into
a usable mlp.cfg file format for the training set.
"""

#outcar_path = sys.argv[1]

#convert_to_db(outcar_path, format='vasp-out')



    
db = connect('cfgs.db')
with open('_train.cfg', 'w') as f:
    for struct in db.select('energy'):
        mapping = set_mapping(sys.argv[1])
        
        lines = write_to_cfg(struct, mapping)
        for line in lines:
            print(line, file=f)
               



    
