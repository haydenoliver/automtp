import os

from ase.db import connect

from mtp import Relax



def struct_to_cfg(struct, mapping):
    cfg_lines = []
    cfg_lines.append("BEGIN_CFG")
    
    # Set the size of the cell
    cfg_lines.append(" Size")
    cfg_lines.append('   ' + str(struct.natoms))

    # Set the supercell lattice vectors
    cfg_lines.append(" Supercell")
    for vec in struct.cell:
        cfg_lines.append('   ' + '\t'.join([str(val) for val in vec]))

    # Set the AtomData values
    cfg_lines.append(" AtomData:  id type   cartes_x  cartes_y  cartes_z  fx  fy  fz")
    atom_num = 1
    for atom in range(len(struct.numbers)): # loop through the 
        atom_num = str(atom + 1) # correcting for the base-0 python index
        
        species = '  ' + str(mapping[struct.numbers[atom]])
        position = '  '.join([str(val) for val in struct.positions[atom]])
        try:        
            forces = '  '.join([str(val) for val in struct.forces[atom]])
        except:
            return
        _atom_line = '\t' + atom_num + '\t' + species + '\t' + position + '\t' + forces

        atom_line = ''.join([str(val) for val in _atom_line])
        cfg_lines.append(atom_line)
    
    # Set energy
    try:
        cfg_lines.append(" Energy")    
        cfg_lines.append('\t' + str(struct.energy))

        cfg_lines.append(" PlusStress: xx   yy   zz   yz   xz   xy")
        cfg_lines.append('\t' + '\t'.join([str(f) for f in struct.stress]))

        
    except:
        pass
    
    cfg_lines.append(" Feature    EFS_by      VASP")
    cfg_lines.append("END_CFG\n")
    return cfg_lines
    

    
def set_mapping(yaml_mapping):
    pass


def db_to_cfg(db_path, cfg_path):
    db = connect(db_path)
    for struct in db.select():
        lines = struct_to_cfg(struct, mapping={27:0, 25:1, 30:2})
        break
    for item in lines:
        print(item)


if __name__ == "__main__":
    db_to_cfg("./cfgs.db", "cfg.out")
