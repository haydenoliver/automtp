#!/bin/bash
iter=`cat iter_num`
mkdir logs/iter_$iter
mkdir logs/iter_$iter/vasp

# Load python environment
source CoZn/bin/activate

echo "============ BEGIN ITERATION $iter ============" | tee -a labBook
echo "Starting step 0" | tee -a labBook
cd vasp
sh ../utils/scripts/run_vasp.sh # Sets up all the files, taking structure info from diff.cfg, and then runs vasp.
sh ../utils/scripts/get_converged.sh
cat converged_poscars.cfg >>../train/train.cfg # Copy successful VASP structures to the training list
rm converged_poscars.cfg


# Recordkeeping
python ../utils/scripts/get_vaspdata.py
cp data.pkl ../logs/iter_$iter/vasp/data.pkl
cp data.csv ../logs/iter_$iter/vasp/data.csv

cd ..
cp vasp/out.cfg logs/iter_$iter/vasp/
cp vasp/diff.cfg logs/iter_$iter/vasp/
cp vasp/mlp.log logs/iter_$iter/vasp/
cp vasp/slurm*.out logs/iter_$iter/vasp/
cp -r vasp/sample_in/ logs/iter_$iter/vasp



conv_cfg=`grep BEGIN vasp/out.cfg | wc -l`
diff_cfg=`grep BEGIN vasp/diff.cfg | wc -l`
echo "$conv_cfg of $diff_cfg structures converged. Transferring to training set..." | tee -a labBook
echo "Finished with step 0. Moving to training step..." | tee -a labBook
echo "" | tee -a labBook
#./step_1_train.sh
