#!/bin/bash

#SBATCH --time=2:00:00   # walltime
#SBATCH --ntasks=8   # number of processor cores (i.e. tasks)
#SBATCH --nodes=1   # number of nodes
#SBATCH --mem-per-cpu=2GB   # memory per CPU core
#SBATCH -J "Training"   # job name



# Set the max number of threads to use for programs using OpenMP. Should be <= ppn. Does nothing if the program doesn't use OpenMP.
export OMP_NUM_THREADS=8

# Get the path to the executable; should be on user's path after the modules have been loaded.
# module purge
# module load libfabric/1.5 mkl/2017 compiler_intel/2017 mpi/impi-2017_intel-2017

mpirun -n 8 mlp train curr.mtp train.cfg --max-iter=500 --trained-pot-name=curr.mtp --curr-pot-name=curr.mtp --stress-weight=5e-4 --force-weight=5e-3
