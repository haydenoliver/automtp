"""
magmom_write.py

This file takes the species data from the __mlip_species file and adds the 
magnetic moment tag "MAGMOM = " to the INCAR. It is very simple right now,
and works exclusively with the Co-Zn system. 

Atom types are listed and worked alphabetically. This takes the atom data 
written explicitly down below. This script reads the __mlip_species data and
writes the atom(s)'s respective magnetic moment to the INCAR file.


############################################################################
Example:
(INCAR)
	MAGMOM = 
There is no written value for MAGMOM here.

(__mlip_species_)
	0 1
This means that there is both atom type 0 and atom type 1 are present in the
system.

Therefore, this script correlates Co to atom type 0 and Zn to atom type 1, 
whose respective magnetic moments are:
	1 0
This ^^^ is how they will be added to the INCAR file. Appended to the end of that
line containing the string:
	MAGMOM = 
and the end result in the INCAR_new file that it creates is
	MAGMOM = 1 0
############################################################################



In the future, I will add functionality to 
automatically take magnetic moment data for any atom, that way this script
can be used generally.

Hayden Oliver
Ames Lab intern
June 13. 2019



Args: 
	none
Returns:
	none


"""






with open("INCAR") as f1:
	incar = f1.readlines()

with open("__mlip_species") as f2:
	mlip_species = f2.read().split()




##############################
####      ATOM TYPES     #####
# for the Co-Zn systems only #
Cobalt = {
	"symbol":"Co",
	"mlipkey":"0",
	"magmom":1
}

Zinc = {
	"symbol":"Zn",
	"mlipkey":"0",
	"magmom":0
}
############################



mlip_magmom = {"0":1, "1":0}

for line in incar:
	if "MAGMOM" in line:
		for species in mlip_species:
			#print(species, type(species))
			#print(mlip_magmom([species]))
			#print(line)
			# we don't want the newline character, so we'll do some string manipulation
			
			tmpline = ' '.join(line.split()[0:2])
			#print(tmpline)
			magmom_line = tmpline + " " + str(mlip_magmom[species]) + "\n"

print(magmom_line)

with open("INCAR_new", 'w') as f:
	for iline in incar:
		
		if "MAGMOM" in iline:
			write_line = magmom_line
		else:
			write_line = iline
###		print(write_line)
		f.write(write_line)
