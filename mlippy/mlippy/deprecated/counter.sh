#!/bin/bash
####################################
####################################
#SBATCH --time=30:00:00   # walltime
#SBATCH --ntasks=1   # number of processor cores (i.e. tasks)
#SBATCH --nodes=1   # number of nodes
#SBATCH --mem-per-cpu=128M   # memory per CPU core
#SBATCH -J "WatchRelax"   # job name



START_TIME=$SECONDS
ELAPSED_TIME=$(($SECONDS - $START_TIME))
echo $ELAPSED_TIME

slurm="slurm-"
ext=".out"
key="4relax"
#key="T2POSCAR"

username=$USER


date > xx.txt
a=`squeue -u $username |grep $key |wc -l`
echo $a

while [ `squeue -u $username |grep $key |wc -l` -gt 0 ] && [ $ELAPSED_TIME -lt 28800 ]
do

   jobID=`squeue -u $username |grep $key | head -n1 | awk '{print $1;}'`
   slurmFile=$slurm$jobID$ext

   isRunning=`squeue -u $username |grep $key | head -n1 | awk '{print $5;}'`
   if [ "$isRunning" == "R" ]
   then
      ##echo "is Running!"
      echo  "---------/\/\/\------------- " $ELAPSED_TIME >> $slurmFile
      ##date >> $slurmFile
      ##echo $slurmFile
   else
      echo "waiting..." >> xx.txt
      ##date >> xx.txt
   fi

   sleep 10

   ELAPSED_TIME=$(($SECONDS - $START_TIME))
done

echo "finished"

####################################
