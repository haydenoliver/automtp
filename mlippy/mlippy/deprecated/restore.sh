#!/bin/bash

# ===========================================================
# CAUTION ==== CAUTION ==== CAUTION ==== CAUTION ==== CAUTION
# ===========================================================

 
# This script will reset your directories, erasing EVERYTHING
# beyond the indicated checkpoint. Be extraordinarily careful
# and don't use this script lightly.


# Execute this checkpoint by giving the iteration checkpoint
# number that you want to start over at. This will erase
# EVERYTHING from the that entire iteration and copy only the 
# diff.cfg and train.cfg files from the iteration beforehand



# ===========================================================
# CAUTION ==== CAUTION ==== CAUTION ==== CAUTION ==== CAUTION
# ===========================================================








# Printout help message
if [ "$1" == "-h" ]; then
  echo "Usage: `basename $0` [somestuff]"
  exit 0
fi

# Initialize parameters
ROOT=`realpath ..`
echo $ROOT
# ROOT=$PWD/empty
checkpoint=$1

# Check if value is okay
if [ -n "$checkpoint" ] && [ "$checkpoint" -eq "$checkpoint" ] 2>/dev/null; then
    echo "input is number. Continuing checkpoint recovery"   
else
    echo "not a number, process exiting now"
    exit 2
fi

# for iters in iter*;
# do
#     echo $iters
#     chkpt_val=0
    
#     if [ $checkpoint -le ${iters: -1} ]; 
#     then
# 	# continue the script if the given checkpoint is not larger than all current iterations
# 	echo ${iters: -1}
# 	echo "input okay"
# 	exit
#     else
# 	echo "exiting"
# 	exit
#     fi
	
# done

# Double check intentions of user
echo "=========== CAUTION ============"
echo "Are you sure you want to do this"
echo "=========== CAUTION ============"

if whiptail --yesno "Are you sure you want to reset to iteration $checkpoint" 20 60 ;then
    sleep 2
else
    echo "Exiting"
    exit
fi
if whiptail --yesno "You're absolutely sure?" 20 60 ;then
    echo "Allllrighty then"
else
    echo "Exiting"
    exit
fi

# Clean root
echo "======= BROKEN RUN =======" >> $ROOT/labBook
echo "Reset to iteration: $checkpoint" >> $ROOT/labBook
mv $/ROOT/labBook logs/iter_$checkpoint
echo $checkpoint > $ROOT/iter_num

# Clean vasp
cd $ROOT/vasp
echo $PWD
rm -r dir_POSCAR*
rm -r POSCAR*
rm diff.cfg
rm data*
rm slurm*

# Clean train
cd $ROOT/train
echo $PWD
rm train.cfg
rm temp.cfg
rm state.mvs
rm curr.mtp
rm slurm*

# Clean relax
cd $ROOT/relax
echo $PWD
rm preselected.cfg
rm selected.cfg
rm curr.mtp
rm state.mvs
rm relaxed.cfg
rm to-relax.cfg
rm xx.txt
rm slurm*

# Clean select
cd $ROOT/select
echo $PWD
rm active_set.cfg
rm preselected.cfg
rm state.mvs
rm selected.cfg
rm slurm*


# Archive logs
cd $ROOT/logs/
reset_num=`cat archived/reset_num`
new_reset=$((reset_num++))
mkdir archived/reset_$new_reset


for iters in iter*;
do
    IFS='_' read -ra iter_split <<< $iters
    iternum=${iter_split[1]}
    
    echo $iternum $checkpoint
    chkpt_val=0
    
    if [ $iternum -ge $checkpoint ]; 
    then
	# echo "Moving dirs to archived/reset_$reset_num/$iters"
	mv $iters archived/reset_$new_reset
    fi
done


# Copying files for checkpoint
cd $ROOT/logs
previous=$(( $checkpoint - 1))
# echo $previous
cp iter_$previous/select/selected.cfg $ROOT/vasp/diff.cfg
cp iter_$previous/train/train.cfg $ROOT/train/train.cfg

if cp iter_$previous/train/curr.mtp $ROOT/train/curr.mtp
then
    echo ""
else
    echo "No potential found. Copying empty potential"
    cp $ROOT/train/empty.mtp $ROOT/train/curr.mtp
fi

cp iter_$previous/relax/to-relax.cfg $ROOT/relax/to-relax.cfg
cp iter_$previous/labBook $ROOT/
echo "DONE"

