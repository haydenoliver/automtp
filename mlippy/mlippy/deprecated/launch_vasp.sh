#!/bin/bash

#SBATCH --time=8:00:00   # walltime
#SBATCH --ntasks=36   # number of processor cores (i.e. tasks)
#SBATCH --nodes=1    # number of nodes
#SBATCH --nodelist node001

#SBATCH --mem-per-cpu=6GB   # memory per CPU core
#SBATCH --cpus-per-task=1

# Set the max number of threads to use for programs using OpenMP.
# Should be <= ppn. Does nothing if the program doesn't use OpenMP.

# source exec_vasp.sh
sh ../utils/scripts/vasp_exec.sh
