#!/bin/python

import sys
# import subprocess
import os

import makeStr


"""This file generates a list of all the derivative structures of the prototype
cells. In this case `bcc, fcc, hcp`. By default the file outputs
the whole set of structures listed in the struct_enum.in file.


Args (sys.argv format):
    sys.argv[0] (str): name of the file [ SET AUTOMATICALLY - NO USER INPUT ]
    sys.argv[1] (str): structure prototype
    sys.argv[2] (str): minimum number of atoms in the cell
    sys.argv[3] (str): maximimum number of atoms in the cell



Examples:
    `python build_structs.py fcc 1 4`
        This example builds directories of the fcc-prototype structure
        containing an input file for makeStr.py to read
        This contains the structures of 1-4 atoms in the
        unit cell (approximately 100 structures total)
"""


def build_configs(prototype='all', minsize=1, maxsize=5):
    if prototype.lower() == 'all':
        prototype = ['fcc', 'bcc', 'hcp']
    else:
        prototype = [prototype]
    os.system('``>all.vasp')

    for proto in prototype:
        if proto.lower() == "hcp":
            str_max = int(maxsize / 2)
            str_min = int(minsize / 2)
            if str_min < 1:
                str_min = 1
            if str_max < 1:
                str_max = 1
        else:
            str_max = maxsize
            str_min = minsize

        # Edit the struct_enum.in file
        with open("prototypes/struct_enum." + proto) as f:
            struct = f.read()
            struct = struct.replace(
                "cell_size_min", str(str_min)
                ).replace("cell_size_max", str(str_max))

            with open(proto + '/struct_enum.in', 'w') as s:
                s.write(struct)
                # parent_path = os.getcwd()
                # os.system("cd " + os.path.join(parent_path, proto) + "&& enum.x")
                # os.system("cd " + proto + " && rm vasp.{} && python ../makeStr.py all -config t -species Co Zn -species_mapping 27 30 && cat vasp.{} >> ../all.vasp")
                # os.rename(parent_path + '/all.vasp', 'configs/full/' + str(minsize) + '_' + str(maxsize) + '.cfg')
                #vasp_target = os.path.join(parent_path, 'all.vasp')
                # 'configs/' + str(minsize) + '__' + str(maxsize) + '.cfg')
		

		

build_configs(minsize=int(sys.argv[1]), maxsize=int(sys.argv[2]))
#sys.argv[1], minsize=int(sys.argv[1]), maxsize=int(sys.argv[2]))
os.system("sh cat_cfgs.sh " + sys.argv[1] + "-" + sys.argv[2])
quit()


			
			
def run_makeStr():
	# Give the arguments for `makeStr.py` as arguments to this executable
	makeStr.run(makeStr._parser_options())
	
#	os.system('python



	







def build_structs(prototype, str_min=1, str_max=10):
	
	print(prototype)

	# cell sizes for HCP (two lattice points, so n/2 is the number of atoms)


	if prototype.lower() == "hcp":
		str_max = int(str_max / 2)
		str_min = int(str_min / 2)
		hcp_tf = True
	else:
		hcp_tf = False
		#str_max = str_max 
		#str_min = str_min
	
	min_size = str_min
	
	print(str_min, str_max)

	for cell_size in range(str_min, str_max + 1):
		with open ("prototypes/struct_enum." + prototype) as f:
			struct = f.read()
		
		max_size = cell_size # from loop condition
		print(min_size, max_size)
		struct = struct.replace("cell_size_min", str(min_size)).replace("cell_size_max", str(max_size))
		
	
		# Set naming of the directories. HCP name will be 2x cell size 
		if hcp_tf == True:
			struct_path = prototype + "/" + str(2 * cell_size)
		else:
			struct_path = prototype + "/" + str(cell_size)
		




		if not os.path.exists(struct_path):
			os.mkdir(struct_path)
	
		with open (struct_path + "/struct_enum.in", 'w') as fw:
			fw.write(struct)
		
	
		enum_cmd = "cd " + struct_path + " && enum.x"
	
		os.system(enum_cmd)
		try:
	   	    	os.remove("vasp.{}")
		except OSError as e:  ## if failed, report it back to the user ##
    			print ("Error deleting previous file. Continuing...")
		
		makeStr_cmd = "( cd " + struct_path + " && python2 ../../makeStr.py all -species Co Zn -config t ; )"
		os.system(makeStr_cmd)
	
	
		
	min_size = max_size
	

#if sys.argv[1] == "all":
#
#	protos = ['fcc', 'bcc', 'hcp']
#	for proto in protos:
#		print(protos)
#		print("Starting enumeration...")
#		
#		build_configs(proto, str_min=sys.argv[2], str_max=sys.argv[3])
#else:
#	build_configs(sys.argv[1], str_min=sys.argv[2], str_max=sys.argv[3])









#build_configs(sys.argv[1]

	



#build_structs(proto, str_min=int(sys.argv[2]), str_max=int(sys.argv[3]))

#else:
#	build_structs(sys.argv[1], str_min=int(sys.argv[2]), str_max=int(sys.argv[3]))


#cwd = os.path.realpath('.')
#print(cwd)

#quit()
#subprocess.check_call(['sh', 'cat_cfgs.sh', str(sys.argv[2]), str(sys.argv[3])], shell=True)
