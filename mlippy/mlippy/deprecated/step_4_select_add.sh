#!/bin/bash

username=$USER
iter=`cat iter_num`
echo "Selecting data from iteration: $iter" | tee -a labBook
echo "Beginning training data selection process..." | tee -a labBook

cp train/curr.mtp select/
cp train/train.cfg select/
cp relax/preselected.cfg select/

# Begin the training data selection step
cd select
rm selected.cfg slurm*.out

node_state=`sinfo -h --nodes=node001 -o "%T"`
if [ "$node_state" == "idle" ]; # if the node is not currently allocated then execute the sbatch script
then
    sbatch launch_select.sh
    while [ `squeue |grep $username |wc -l` -gt 0 ];
    do
	squeue |grep $username |wc -l; sleep 10;
    done
else
    echo "Node busy, running select step interactively..." | tee -a ../labBook
    mpiexec -n 4 mlp select-add curr.mtp train.cfg preselected.cfg selected.cfg --select-threshold=2.0 --nbh-weight=0.0 --energy-weight=1.0 --mvs-filename=state.mvs --selected-filename=active_set.cfg --select-limit=200 > slurm-interactive.out
fi


cp selected.cfg ../vasp/diff.cfg


echo " -- Step 4 output -- " | tee -a ../labBook
cat slurm*.out >> ../labBook

echo `date` >> labBook
echo " ======== END ITERATION $iter ======== " | tee -a ../labBook


cd ../

# Recordkeeping
mkdir logs/iter_$iter/select
cp select/preselected.cfg logs/iter_$iter/select/
cp select/state.mvs logs/iter_$iter/select/
cp select/active_set.cfg logs/iter_$iter/select/
cp select/slurm-*.out logs/iter_$iter/select/
cp select/selected.cfg logs/iter_$iter/select/

cp labBook logs/iter_$iter/labBook_$iter


echo "iter = $((iter++))"
echo  $iter > iter_num
echo "Next iteration is: $iter" | tee -a labBook

cat iter_num

echo "done"


# ./step_0_vasp.sh
