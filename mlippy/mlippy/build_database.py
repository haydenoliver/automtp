import os
import numpy as np
import time

from ase.calculators.singlepoint import SinglePointCalculator
from ase import Atoms, Atom
from ase.db import connect

import argparse
import yaml


# Set up the argument parser

parser = argparse.ArgumentParser(description='')

parser.add_argument('--cfg_path', metavar='', type=str,
                    help='an integer for the accumulator')
parser.add_argument('--species', metavar='', nargs='+', 
                    help='Provide the species mapping')
parser.add_argument('--yaml', metavar='', type=str,
                    help='Provide the path the system yaml config file')
parser.add_argument('--calculator', metavar='', type=str,
                    help='Provide the name of the ase.calculator to attach')


args = parser.parse_args()
#print(args.accumulate(args.integers))


def extract_cfg(cfg_path, species_map, selected=True):
    """
    Reads in an mlip style .cfg file, parses each structure into an ASE atoms object.
    It currently reads the file in line by line which is very memory efficient.
    However, all the structures are 

    Args:
    cfg_path (str): Path to the database file
    species_map (dict): Mapping of the structures using the MLIP standard
    selected (bool): T/F if the input file is from the selected step of the MLIP training routine
        Options:
            True: Structures are the output from the select step. Energies and forces will not be
                  added to their entries in the database.
            False: Structures are not from a selection step, and therefore the calc object will be
                  initialized and record the energy and forces

    """

    mlip_data = []
    extra_data = []
    
#    if yaml_path is not None:
#        with open(yaml_path, 'r') as yml:
#            system_yaml = yaml.safe_load(yml)

    with open(cfg_path, 'r') as infile:
        counter = 0
        crystal_set = []
        features = {}
        for line in infile:
            if 'BEGIN_CFG' in line:
                continue
                    
            if 'Size' in line:
                cell_size = int(next(infile))
                continue

            if 'supercell' in line.lower():
                cell_vecs = []
                cell_vecs.append([float(i) for i in next(infile).split()])
                cell_vecs.append([float(i) for i in next(infile).split()])
                cell_vecs.append([float(i) for i in next(infile).split()])
                continue

            if 'AtomData' in line:
                atoms = []
                forces = []
                for i in range(cell_size):
                    adata = [float(f) for f in next(infile).split()]
                    num = int(adata[0])
                    mlip_atom_type = int(adata[1])
#                    print(mlip_atom_type)
#                    print(species_map)
                    species = species_map[mlip_atom_type]
                    position = adata[2:5]
                    forces.append(adata[5:8])
#                    magmom = system_yml['magmom'][mlip_atom_type]
                    atoms.append(Atom(species, position, index=num))#, magmom=magmom))
                    
                    continue

            if 'Energy' in line:
                energy = float(next(infile))
                continue

            if 'PlusStress' in line:
                plusstress = [float(i) for i in next(infile).split()]
                continue

            if 'conf_id' in line:
                conf_id = int(line.split()[2])
                continue

            if 'Feature' in line and 'relaxation' not in line:
                feat_type = line.split()[1]
                feat_val = line.split()[2]
                #             print(feat_type, feat_val)
                features[feat_type] = feat_val
                continue

            elif 'relaxation' in line:
                relax = line.split()[2].split('_')
                relax_state = relax[1]
                features[relax[0]] = relax_state
                continue

            if 'END_CFG' in line:
                """ Create the Atoms object from the array `atoms` created earlier.
                All MLIP tags are stored in `info`

                """
#                struct_info = {
#                    'energy':energy,
#                    'forces':forces,
#                    'stress':plusstress,
#                }
                extra_data.append({'conf_id':conf_id, **features})
                
#                mlip_data.append(struct_info)

                crystal = Atoms(atoms, pbc=True)
                crystal.set_cell(cell_vecs)                
                if selected == False:
                    energy
                    calc = SinglePointCalculator(crystal,
                                             energy=energy,
                                             forces=forces,
                                             stress=plusstress
                    )
                    crystal.set_calculator(calc)
                else:
                    pass
                # INITIALIZE CRYSTAL PROPERTIES
                
#                for atm in crystal.get_
#                crysta.set_initial_magnetic_moments(magmoms)

                crystal_set.append(crystal)
    return crystal_set

def attach_calculator():
    pass


def write_database(ase_objs, db_path="."):
    db_out_path = os.path.join(db_path, 'cfgs.db')
    print(db_out_path)
    with connect(db_out_path) as db:
        for i in range(len(ase_objs)):
            db.write(ase_objs[i],
                     # data=extra_data[i]
            )
#            print(ase_objs[i].get_initial_magnetic_moments())


if __name__ == "__main__":

    with open('write_db.txt', 'r') as f:
        write_db_Q = f.read().split()[0]

    # 1 means done
    # 0 means we need to add structures to the datatase
    if write_db_Q == '1':
        print("Structures already in Database. Exiting...")
        exit()
    else:
        print("Selected configuration not yet in training database")
        print("Adding to database...")
        with open('write_db.txt', 'w') as w:
            print('1', file=w)
        




    
#    print(args)
#    quit()
    if args.yaml is not None:
#        print(args.yaml,type(args.yaml))
        with open(args.yaml, 'r') as y:
            system_yml = yaml.safe_load(y)
            
        species_map = system_yml['species']
    #        cfg_path = os.path.join(yml['ROOT'],yml['path']['struct_path'])
    else: #argparse.species is not None:
        species_map = {str(i):vals[i] for i in range(len(vals))}
    cfg_path = os.path.join(os.getcwd(), args.cfg_path)

    # Read in the cfg and build atoms object list
    print(species_map, type(species_map))
    structures = extract_cfg(cfg_path, species_map, selected=True)

    if args.calculator:
        for struct in structures:
            calc = argparse.calculator
            # struct.set_calculator(argparse.calculator)
            if calc.lower() == "vasp2" or calc.lower() == "vasp":
                pass
    write_database(structures, db_path=os.getcwd())
