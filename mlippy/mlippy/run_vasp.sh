#!/bin/bash

# We start with the diff.cfg file. Structures in this file will be VASPed
ROOT=$PWD
echo $ROOT

username=$USER

rm -rf dir_POSCAR*;
rm -f POSCAR*

##module load mpi/openmpi-1.8.5_intel-15.0.2;
##module load mpi/openmpi-x86_64 # this is the openmpi on Wukong. Is it possible to run without it?
mlp convert-cfg diff.cfg POSCAR- --output-format=vasp-poscar;

for f in POSCAR*; do
    if [ ! -d dir_$f ];
    then
	echo dir_$f;
	cp -rv $ROOT/sample_in/ dir_$f/;
	mv $f dir_$f/POSCAR;

	# fi;
	
	cd dir_$f;

	# rm OUTCAR
	# if [ ! -f OUTCAR ];
	# then
	mv POSCAR _POSCAR;
	echo jobnumber: ${f:6:4};
	mv POTCAR _POTCAR;
	../__mlip_pre.pl;
	echo $PWD;
	# python ../magmom_write.py
	# mv INCAR_new INCAR
	getKPoints   
	# kpoints.x
	#exit
	# sbatch -J CoZn-${f:6:4} $ROOT/launch_vasp2.sh; ##this is for job summission without a job array
	####  			 satch -J CoZn-${f:6:4} $ROOT/launch_vasp.sh;


	cd $ROOT;
    fi;
done

cd $ROOT


# Launch the vasp jobs either on SBATCH or interactively
node_state=`sinfo -h --nodes=node001 -o "%T"`
if [ "$node_state" == "idle" ]; # if the node is not currently allocated then execute the sbatch script
then
    ####    { /usr/bin/time -f "\n%e real\n%U user\n%S sys" sbatch $ROOT/launch_vasp.sh ; } 2> sbatch_timing
#    max_cores=36
    max_cores=64
    export max_cores
    #sbatch $ROOT/launch_vasp.sh 
    sh $ROOT/../utils/scripts/vasp_exec.sh | tee slurm-interactive.out
    
    while [ `squeue | grep $username | wc -l` -gt 0 ];
    do
	echo "Running SBATCH";
	sleep 30;
    done
else
    cd $ROOT
    max_cores=64
    export max_cores
    sh $ROOT/../utils/scripts/vasp_exec.sh | tee slurm-interactive.out
    
fi

# tail -n 3 *timing > time.out
# cat time.out >> ../labBook

