# READ THE DOCUMENTATION FOR ENUMLIB

#from io import StringIO, BytesIO, FileIO
import argparse
import io
from numpy import array
import os
from random import uniform
import subprocess as sp
import yaml
from multiprocessing import cpu_count, Process

from ase import Atoms
from ase.db import connect
from ase.io import read as read_ase
from ase.build import bulk

from mlippy.makeStr import *
from mlippy.makeStr import _read_enum_out, _map_enumStr_to_real_space, _cartesian2direct, _get_lattice_parameter


def parse_args():
    

    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('yaml', nargs='+',
                        help='One or more yaml files ready for enumeration')
    parser.add_argument('--clear', type=bool,
                        help='Clear previous database or not')

    args = parser.parse_args()
    return args


class Enumerate():
    """
    Contains all the parameters necessary to enumerate bulk structures for training an MTP

    TODO: Add capability to handle restrictions on lattice points



    makeStr default arguments:
    default_args = {
      'examples': False,
      'verbose': False,
      'action': 'print',
      'debug': False,
      'structures': None, #['all']
     'displace': 0.0,
      'input': 'struct_enum.out',
      'mink': 't',
      'species': [],
      'species_mapping': [],
    #    'outfile': 'vasp.{}',
      'rattle': 0.0,
      'config': 'f',
      'remove_zeros': 'f'
    }        
    """
    def __init__(self, yaml_path):
        self.read_yaml(yaml_path)
        self.SetupProcess()
        self.clear = False

    def read_yaml(self, yaml_path):
        """Read the configuration yaml file for the alloy database"""

        with open(yaml_path) as yp:
            yml = yaml.safe_load(yp)

        self.species = list(yml['species'].values())
        print(self.species)
        self.nspecies = len(self.species)
        alloy = ''.join(self.species)


        self.database_path = os.path.join(yml['ROOT'], alloy)
        self.setup_path = os.path.join(self.database_path, 'setup')

        
        self.__dict__.update(yml['enumeration'])
        

    def ClearDatabase(self):
        enum_db_path = os.path.join(self.setup_path, 'enum.db')
        if os.path.isfile(enum_db_path):
            os.remove(enum_db_path)
        
    def SetupProcess(self):
        """
        Using the `ase.build.bulk` module, we calculate the lattice vectors and lattice points 
        that will be used in the enumeration.

        
        """
        self.Process = []
        
        for parent in self.lattice:
            enum_path = os.path.join(self.setup_path, parent)
            struct_enum = os.path.join(enum_path, 'struct_enum.in')
            
            if not os.path.isdir(enum_path):
                os.mkdir(enum_path)

            struct = bulk('X', parent, a=1)
            npoints = len(struct.positions)
            
            with open(struct_enum, 'w') as f:
                print('Writing input for:', parent)
                print(parent, file=f)
                print('bulk', file=f)

                for vec in struct.cell: 
                    print(*vec, sep=' ', file=f)

                print(self.nspecies, '-nary case', file=f)
                print('  ', npoints, '  # number of points in multilattice', file=f)
                
                for pos in struct.positions:
                    print(*pos, self.restrictions, sep=' ', file=f)
                
                self.cell_size[1] = int(self.cell_size[1] / npoints)
                print(*self.cell_size, sep=' ', file=f)
                print(self.finite_precision, file=f)
                print(self.labelings, file=f)
                
                try:
                    for conc in self.concentrations:
                        print(conc, file=f)
                except TypeError:
                    print('No concentration restrictions applied')

            # initizlize the Processes
            #    print(enum_path, "ENUM_PATH")
            #self.ClearDatabase()
            self.Process.append(Process(target=self._EnumerateStructs, args=(enum_path,)))
            
    def _EnumerateStructs(self, enum_path):
        """
        Worker process for enumeration and generating the atoms database
        """
        with open(os.path.join(enum_path, 'enum.out'), 'wb') as f:
            # Cleanup before enum.x
            my_env = os.environ.copy()

            sp.run(['rm',
                    'enum.out',
                    'vasp.cfg'],
                   cwd=enum_path,
                   stderr=sp.PIPE,
                   stdout=sp.PIPE,
                   env=my_env
            )
            
            # Run enum.x
            print('Running: enum.x')
            sp.run(['enum.x'], cwd=enum_path, stderr=f, stdout=f, env=my_env)

        # Convert the `struct_enum.out` file into an Atoms database
        #args = self.makeStr
        self.makeStr['species'] = self.species
        #        args['path'] = enum_path
        #        print(args)
        
#        for key in self.makeStr:
#            print(key, type(self.makeStr[key]))
        make_database(enum_path, self.makeStr)
        

    def RunEnum(self):
        for p in self.Process:
            p.start()
        for p in self.Process:
            p.join()
        


    
# Write the structures to an ASE object using the makeStr.py template from writing poscars

default_args = {
#    'examples': False,
    'verbose': False,
    'action': 'print',
    'debug': False,
    'structures': None, #['all']
    'displace': 0.0,
    'input': 'struct_enum.out',
    'mink': 't',
    'species': [],
#      'species_mapping': [],
#    'outfile': 'vasp.{}',
    'rattle': 0.0,
    'config': 'f',
    'remove_zeros': 't'
}        


def make_database(enum_path, args):
    """Writes a vasp POSCAR style file for the input structure and system
    data.
    :arg system_data: a dictionary of the system_data
    :arg space_data: a dictionary containing the spacial data
    :arg structure_data: a dictionary of the data for this structure
    :arg args: Dictionary of user supplied input.

    """

    os.chdir(enum_path)
    (system, structure_data) = _read_enum_out(args)
    atoms = []

    for structure in structure_data:
        # space_data is a dictionary containing the spacial data for
        # the structure

        space_data = _map_enumStr_to_real_space(system,structure,args["mink"])
        space_data["aBas"] = _cartesian2direct(space_data["sLV"],
                                                  space_data["aBas"],
                                                  system["eps"])

        atom = _write_ASE(system, space_data, structure, args)
        atoms.append(atom)
    database_path = os.path.join('..', 'enum.db')
    with connect(database_path, append=True) as db:
        for struct in atoms:
            db.write(struct)


def _write_ASE(system_data,space_data,structure_data,args):
    """Writes a string in vasp POSCAR style for the input structure and system
    data and then stores that that data as an ASE Atoms object.
    Code adapted from `Enumlib.makeStr.py`.

    :arg system_data: a dictionary of the system_data
    :arg space_data: a dictionary containing the spacial data
    :arg structure_data: a dictionary of the data for this structure
    :arg args: Dictionary of user supplied input.
    """
#    print('STARTING _write_ASE', '.............................')
#    from numpy import array
#    from random import uniform

#  from mlippy.makeStr import _get_lattice_parameter
    
    # Get the output file name.
    #if "{}" in args["outfile"]:
    #    filename = args["outfile"].format(str(structure_data["strN"]))
    #else:
    #    filename = args["outfile"] + ".{}".format(str(structure_data["strN"]))
    filename = "poscar"
    
    # Get the labeling, group index, structure number and arrow labels
    # from the input data structure.

    labeling = structure_data["labeling"]
    gIndx = space_data["gIndx"]
    arrows = structure_data["directions"]
    struct_n = structure_data["strN"]

    # The arrow basis.
    arrow_directions = [[1,0,0],[-1,0,0],[0,1,0],[0,-1,0],[0,0,1],[0,0,-1]]
    directions = []

    # Construct the concentrations of the atoms from the labeling by
    # counting the number of each type of atom present in the
    # labeling.
    concs = []
    for i in range(system_data["k"]):
        this_conc = 0
        for atom in range(structure_data["n"]*system_data["nD"]):
            if labeling[gIndx[atom]] == str(i):
                this_conc += 1
        concs.append(this_conc)
    def_title = '' #"{} str #: {}".format(str(system_data["title"]),str(structure_data["strN"]))
    # Get the lattice parameter for the atomic species provided by the
    # user.
    lattice_parameter, _title = _get_lattice_parameter(args["species"],concs,
                                                      system_data["plattice"],system_data["nD"],
                                                      def_title,remove_zeros=args["remove_zeros"])

    title = ' '.join(_title.strip('\n').split())
    for arrow in arrows:
        directions.append(array(arrow_directions[int(arrow)]))

    sLV = space_data["sLV"]

    # Start writing the data to the file.
    with io.StringIO() as poscar:
        # First write the title and the lattice parameter.
        print(title, file=poscar)
        print(lattice_parameter, file=poscar)

        # Then write out the lattice vectors.
        for i in range(3):
            print(*sLV[i], sep='  ', file=poscar)

        cell = sLV


        # Write the concentrations to the output file. If the species
        # strings were passed in by the user and the user requests
        # there be no zeros in the concentration string then we should
        # remove them from the file. Otherwise we default to leaving
        # them in.
        if (args["remove_zeros"] and args["species"] is not None):
        #    print(concs, "CONCS")
            conc_str = ''
            for ic in concs:
                if ic != 0:
                    poscar.write("{}   ".format(str(ic)))
        
#                print(*concs, sep='  ', file=poscar)

        print("\nD", file=poscar)
        # Now write out the atomic positions to the file.
        for ilab in range(system_data["k"]):
            for iAt in range(structure_data["n"]*system_data["nD"]):
                rattle = uniform(-args["rattle"],args["rattle"])
                displace = directions[iAt]*args["displace"]*lattice_parameter
                # If the displacement is non zero and we're `rattling`
                # the system then we need to modify the displacement
                # by the amount being rattled.
                displace += displace*rattle
                if labeling[gIndx[iAt]] == str(ilab):
                    # The final atomic position is the position from
                    # the basis plus the total displacement.
                    out_array = array(space_data["aBas"][iAt]) + displace
                    poscar.write(" {}\n".format(
                        "  ".join(["{0: .8f}".format(i) for i in out_array.tolist()])))
        
                    #        _poscar = poscar.getvalue()
        _poscar = poscar.getvalue()
#    print(_pos)
    atom = read_ase(io.StringIO(_poscar), format='vasp')
    return atom

def main():
    args = parse_args()

    for alloy in args.yaml:
        print("Enumerating:", alloy)
        enum_alloy = Enumerate(alloy)
        enum_alloy.ClearDatabase()
        enum_alloy.RunEnum()


if __name__ == "__main__":
    main()
