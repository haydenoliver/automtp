#!/bin/bash

# timing information
start=`date +$s`

ROOT=$PWD
user=$USER


# check that we are in the vasp directory
if [ ${ROOT##*/} != vasp ] ;
then 
    echo "Not in vasp"
    echo $ROOT
#	exit
else
    echo $PWD
fi


# Set total number of cores to use
#if [ `squeue | grep $holiver | wc -l` -gt 0 ];
#then
#    echo $max_cores	
#    #max_cores=36
#else
#    max_cores=64
#    echo $max_cores
#fi
echo Cores used: $max_cores > cores.tmp
#exit

# Set number of process to run each job with
mpi_cores=8

# Calculate the max number of jobs to run concurrently
max_jobs=$(( $max_cores / $mpi_cores ))
echo "Max jobs = $max_jobs"

n_running_jobs=0;
n_structs=`grep BEGIN diff.cfg | wc -l`
echo "Number of structures: $n_structs"


	

for i in $(seq 1 $n_structs);
do
    n=$(expr $i - 1);
    # n_running_jobs=`ps aux | grep holiver | grep bin | grep vasp_toy | wc -l`
    n_running_jobs=`pgrep -c mpiexec`
    echo "Jobs running: $n_running_jobs"
    

    
    while [ $n_running_jobs -ge $max_jobs ];
    do
	echo "$n_running_jobs currently running... "
	echo "Waiting for process to finish before beginning next one..."
	echo ""
	sleep 10
	# n_running_jobs=`ps aux | grep holiver | grep bin | grep vasp_toy | wc -l`
	n_running_jobs=`pgrep -c mpiexec`
    done

    # mkdir toy$n;    
    # (cd toy$n && ../vasp_toy.sh > vasp.out) &
    (cd dir_POSCAR-$n && mpiexec -n $mpi_cores vasp_ncl > vasp.out) &
    children[$n]=$!
    echo "pid: $children[$n]"
done

echo $children
# wait for serial jobs to finish
for pid in ${children[*]} ;
do
    echo "Waiting on pid:$pid ..."
    echo "Still running: "`pgrep -c mpiexec`
    echo `date`
    wait $pid
    

done

end=`date +$s`
total_time=$(( $end - $start))
echo $total_time
