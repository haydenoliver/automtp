#!/bin/bash
#
#
# This script, when run, will ALWAYS target the setup directory of this MTP
# It will concatenate all the config files from different prototype structures
# with identical cell sizes. These are generated beforehand by makeStr.py
#
#

# Set the source directory as the absolute path of the script
# DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

makeStr=$1
SPECIES=$2
MAPPING=$3
# cd $DIR;

# echo working directory;
echo $PWD

rm configs/$1.cfg
n=1
for proto in hcp fcc bcc; do
    # echo $i;
    echo $proto;
    cd $proto;
    
    # Remove previous vasp.{} configs
    rm vasp.{} struct_enum.out debug_* symops_enum* readcheck*

    # enumerate the designated structures
    enum.x

    # Build all structs
    (python $makeStr all -config t -species $SPECIES -species_mapping $MAPPING; ) &
    children[n]=$!
    # python ../makeStr.py all -config t -species Co Zn ; 

    cd $DIR
    n=$(( n + 1))
    # cat $proto/vasp.{} >> configs/$1.cfg

done
# exit

for pid in ${children[*]} ;
do
    echo "Waiting on pid: $pid";
    wait $pid;
done

cd $DIR;
cat bcc/vasp.{} fcc/vasp.{} hcp/vasp.{} > configs/$1.cfg;
echo `grep BEGIN configs/$1.cfg | wc -l` "structs in $1";





exit
# counter=2;
# cd configs;
# for struct in {2..10}; do
    # echo $struct;
    # for num in {1..$struct}; do

	# echo $num;
	# echo $counter;
	# if [ $num -le $struct ]; then
	    # cat simple/atom_$struct >> compound/cmp_$struct.cfg
	    
	    # echo ;
            
	# fi;
	# ((counter++))
# done;
# done


# END=10
# for ((i=1; i<=END; i++)); do
    # for struct in {1..$i}; do
	# cat simple/atom_$struct >> compount/cmp_$struct.cfg;
	# echo $struct;
    # done
# done
# echo $PWD;
# cd configs;
# rm compound/*;

# for i in $(seq 10 $END);
# do
    # for j in $(seq $i $END);
    # do 
	# echo $i $j
	# cat simple/$j.cfg >> compound/$i.cfg;
    # done;
# done;
