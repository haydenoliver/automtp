#!/bin/bash

module purge
module load python/3.7 gcc/8 openmpi/4.0


username=$USER
# home=$HOME
iter=`cat iter_num`
root=`pwd`
amtp_dir=$HOME/automtp
root_mtp=${PWD##*/} 

# Step 1 -- TRAIN
cd vasp
echo '0' > write_db.txt
python $amtp_dir/mlippy/src/convert_cfgs.py $root_mtp.yaml

cd $root/train

cp $root/vasp/_train.cfg train/train.cfg



# Step 0  -- VASP Calculations - RUN LAST
python ~/automtp/mlippy/src/build_database.py --yaml ../../AlCoW.yaml --cfg_path ../select/selected.cfg
python ~/automtp/mlippy/src/run_vasp_db.py


