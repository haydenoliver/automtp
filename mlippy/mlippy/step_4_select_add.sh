#!/bin/bash

username=$USER
iter=`cat iter_num`
echo "Selecting data from iteration: $iter" | tee -a labBook
echo "Beginning training data selection process..." | tee -a labBook

cp train/curr.mtp select/
cp train/train.cfg select/
cp relax/preselected.cfg select/

# Begin the training data selection step
cd select
rm selected.cfg slurm*.out

slurm_submit_string=`sbatch launch_select.sh`
set -- $slurm_submit_string
job_id=$4
while [ `squeue |grep -c $job_id` -gt 0 ];
do
    echo "Waiting on selection: $job_id";
    sleep 5;
done

#cp selected.cfg ../vasp/selected.cfg
#python ~/automtp/mlippy/src/convert_cfgs.py



echo " -- Step 4 output -- " | tee -a ../labBook
cat slurm*.out >> ../labBook

echo `date` >> ../labBook
echo " ======== END ITERATION $iter ======== " | tee -a ../labBook


cd ../

# Recordkeeping
mkdir logs/iter_$iter/select
cp select/preselected.cfg logs/iter_$iter/select/
cp select/state.mvs logs/iter_$iter/select/
cp select/active_set.cfg logs/iter_$iter/select/
cp select/slurm-*.out logs/iter_$iter/select/
cp select/selected.cfg logs/iter_$iter/select/

cp labBook logs/iter_$iter/labBook_$iter


echo "iter = $((iter++))"
echo  $iter > iter_num
echo "Next iteration is: $iter" | tee -a labBook

cat iter_num

echo "done"





./step_0_vasp.sh
