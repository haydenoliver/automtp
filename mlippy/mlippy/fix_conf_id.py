import os
import sys
import in_place




def fix_conf_id(path):


    filepath = path

    counter = 0
    with in_place.InPlace(filepath) as f:
        for line in f:
            if 'conf_id' in line:
                line = ' '.join(line.split()[0:2])
                print(line, '\t', counter, file=f, end='\n')
                counter += 1
            else:
                print(line, file=f, end='')

if __name__ == '__main__':
    fix_conf_id(sys.argv[1])
