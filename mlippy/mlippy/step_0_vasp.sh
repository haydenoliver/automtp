#!/bin/bash
iter=`cat iter_num`

alloy=${PWD##*/}
root=$PWD

mkdir logs/iter_$iter
mkdir logs/iter_$iter/vasp

module purge
module load python/3.7 gcc/8


echo "============ BEGIN ITERATION $iter ============" | tee -a labBook
echo "Submitting VASP jobs" | tee -a labBook

cd vasp

python ~/automtp/mlippy/mlippy/build_database.py --yaml $root/../$alloy.yaml --cfg_path $root/select/selected.cfg
python ~/automtp/mlippy/mlippy/run_vasp_db.py
